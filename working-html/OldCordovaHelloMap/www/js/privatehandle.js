//once new research is pressed the data will be cleared.
$('#newResearch').click(function(){
    location.href= 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function(){
    location.href= 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
});

$('#account').click(function(){
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    console.log(userId);
    location.href = "private.html";
});

// Generates research id
function generateResearchId(){
    return Math.random().toString(36).substr(2, 9);
}

function handleRun(button_id) {
    var researchId = button_id.replace(/(?:\r\n|\r|\n)/g, '');
     console.log(researchId);

    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/requestresearch.php',
        data: {postRequestResearchId:researchId},
    success: function(response, status, xhr) {
        var responseClean = response.replace(/(?:\r\n|\"|\]|\[|\r|\n)/g, '');
        var responseArray = responseClean.split("-");  
        
        var researchName = responseArray[0];
        var researchBtns = responseArray[1];
        var researchBtnsArray = researchBtns.split(",");
        var researchBtnsNames = responseArray[2];
        var researchBtnsNamesArray = researchBtnsNames.split(",");
        var researchBtnsLongNames = responseArray[3];
        var researchBtnsLongNamesArray = researchBtnsLongNames.split(",");
        
        console.log(researchName, researchBtnsArray, researchBtnsNamesArray, researchBtnsLongNamesArray);
        
        
        var oldButtons = researchBtnsArray;
        var oldButtonsName = researchBtnsNamesArray;
        var oldButtonsDesc = researchBtnsLongNamesArray;
        var finalButtonArray = [];
        var finalButtonDescArray = [];
        
        localStorage.setItem('researchName', JSON.stringify(researchName));
        localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
        localStorage.setItem('researchId', researchId);

        setTimeout(function() {
            if (oldButtons.length >= 1) {
            location.href = 'research.html';
            for (var i = 0; i < oldButtons.length; i++) {
                finalButtonArray.push(oldButtons[i]);
                finalButtonArray.push(oldButtonsName[i]);
                finalButtonDescArray.push(oldButtons[i]);
                finalButtonDescArray.push(oldButtonsDesc[i]);
            }
            localStorage.setItem('finalButtonArray', JSON.stringify(finalButtonArray));
            localStorage.setItem('finalButtonDescArray', JSON.stringify(finalButtonDescArray));
        } else {
            document.getElementById("errorPane").style.visibility = "visible";
            document.getElementById("errorMsg").style.visibility = "visible";
            document.querySelector("#errorMsg").innerHTML = "Error: Please add atleast two buttons.";
        }
    },3000);

    },
    error: function(xhr, status, error) {
        console.log("here3");
        console.log(xhr);
        console.log(status);
        console.log(error);
    }});
    return false; 
}

function handleView(button_id, researchName) {
    var researchId = button_id.replace(/(?:\r\n|\r|\n)/g, '');
    localStorage.setItem('viewResearchName', JSON.stringify(researchName));
    localStorage.setItem('viewResearchId', JSON.stringify(researchId));
    location.href = "researchview.html";
}

$('#refresh').click(function(){
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    console.log(userId);
    location.href = "private.html";
});