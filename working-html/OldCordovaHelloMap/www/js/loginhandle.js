//once home is pressed the data will be cleared.
$('#homePage').click(function() {
    location.href = 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('researchedDataArray', '[]');
});

$("#loginButton").click(function () {
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
    postLogin();
});

$("#register").click(function () {
    location.href = "register.html";
});

$(function() {
    $("#username").keydown(function(e) {
        if(e.keyCode==13) {
            document.getElementById("password").focus();
        }
    });
     $("#password").keydown(function(e){
        if(e.keyCode==13) {
            document.getElementById("loginButton").focus();
        }
    });
});

function postLogin() {     
    var username = $('#username').val();
    var password = $('#password').val();
       
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/login.php',
        data: {postUsername:username,postPassword:password},
    success: function(response, status, xhr) {
        var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
        localStorage.setItem('loginReply', JSON.stringify(responseClean));
        localStorage.setItem('username', JSON.stringify(username));
        //reply to the result div tag
        if(responseClean === "error") {
            document.getElementById("errorPane").style.visibility = "visible";
            document.getElementById("errorMsg").style.visibility = "visible";
            document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Login failed.";
        } else if(responseClean.indexOf("private.html") > -1){
            var responseArray = responseClean.split(" ");
            localStorage.setItem('userId', JSON.stringify(responseArray[1]));
            document.getElementById("errorPane").style.visibility = "hidden";
            document.getElementById("errorMsg").style.visibility = "hidden";
            location.href = responseArray[0];
        }
        
    },
    error: function(xhr, status, error) {
        console.log("here3");
        console.log(xhr);
        console.log(status);
        console.log(error);
    }});
    return false; 
}
