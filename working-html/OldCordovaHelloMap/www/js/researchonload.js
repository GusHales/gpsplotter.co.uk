function onLoad() {
    document.addEventListener("deviceready", onDeciveReady);
    
}

function onDeciveReady() {
//window.onload = function() {
    document.addEventListener("backbutton", onBackKeyDown, false);
    var resName = JSON.parse(localStorage.getItem('researchName')) || [];
    var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [];
    var btnName = JSON.parse(localStorage.getItem('finalButtonArray')) || [];
    var btnDesc = JSON.parse(localStorage.getItem('finalButtonDescArray')) || [];
    var btn = ""; 
    var splitValue = "";
    
    console.log(resName ,oldButtons, btnName, btnDesc);

    document.getElementById('researchId').value = localStorage['researchId'];
    document.getElementById('researchNameHeader').value = resName;    
    document.querySelector('#researchNameHeader').innerHTML = "Research: "+resName;
    
    for(var i = 0; i < oldButtons.length; i++) {  
    if(oldButtons.indexOf("btn1") != -1){
        $("#btn1").show();
        document.querySelector('#btn1').innerHTML = btnName[btnName.indexOf("btn1")+1];
        }
    if(oldButtons.indexOf("btn2") != -1){
        $("#btn2").show();
        document.querySelector('#btn2').innerHTML = btnName[btnName.indexOf("btn2")+1];
        }
    if(oldButtons.indexOf("btn3") != -1){
        $("#btn3").show();
        document.querySelector('#btn3').innerHTML = btnName[btnName.indexOf("btn3")+1];
        }
    if(oldButtons.indexOf("btn4") != -1){
        $("#btn4").show();
        document.querySelector('#btn4').innerHTML = btnName[btnName.indexOf("btn4")+1];
        }
    if(oldButtons.indexOf("btn5") != -1){
        $("#btn5").show();
       document.querySelector('#btn5').innerHTML = btnName[btnName.indexOf("btn5")+1];
        }
    if(oldButtons.indexOf("btn6") != -1){
        $("#btn6").show();
        document.querySelector('#btn6').innerHTML = btnName[btnName.indexOf("btn6")+1];
        }
    }
    //get button name and desc
    $('#btn1, #btn2, #btn3, #btn4, #btn5, #btn6').focusin(function () {
        if (this.id == 'btn1') {
            btn =  document.querySelector('#btn1').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn1")+1];
        }
        else if (this.id == 'btn2') {
            btn = document.querySelector('#btn2').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn2")+1];
        }
        else if (this.id == 'btn3') {
            btn = document.querySelector('#btn3').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn3")+1];
        }
        else if (this.id == 'btn4') {
            btn = document.querySelector('#btn4').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn4")+1];
        }
        else if (this.id == 'btn5') {
            btn = document.querySelector('#btn5').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn5")+1];
        }
        else if (this.id == 'btn6') {
            btn = document.querySelector('#btn6').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn6")+1];
        }
    });
};

// Handle the back button
//
function onBackKeyDown() {
    location.href = "research.html";
}

setTimeout(function(){
    document.getElementById("pageone").style.visibility = "visible";
}, 100);

