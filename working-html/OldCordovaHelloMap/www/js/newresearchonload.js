
function onLoad() {
    document.addEventListener("deviceready", onDeviceReady);
}
//on refresh the data wount be lost
function onDeviceReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
    document.getElementById("errorPane").style.visibility = "hidden";
    document.getElementById("errorMsg").style.visibility = "hidden";
    document.getElementById('researchId').value = localStorage['researchId'];

    var researchName = JSON.parse(localStorage.getItem('researchName')) || [];
    var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [];
    var oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [];
    var oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [];
    
    var formData = {};

    if(researchName != "") {
        document.getElementById('researchName').value = researchName;
        document.getElementById('researchName').readOnly = true;
        console.log(researchName);
    }
    for(var i = 0; i < oldButtons.length; i++ ){
        formData["buttons"] = oldButtons[i];
        formData["buttonName"] = oldButtonsName[i];
        formData["buttonDesc"] = oldButtonsDesc[i];
        formData["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';

        var row = $('<tr></tr>');
            $.each(formData, function( type, value ) {
                $('<td class="input-'+type+'"></td>').html(value).appendTo(row);
            });
            $('.preview-table > tbody:last').append(row); 
    }
}

// Handle the back button
//
function onBackKeyDown() {
    location.href = "private.html";
}
onDeviceReady();

setTimeout(function(){
    document.getElementById("pageone").style.visibility = "visible";
}, 100);