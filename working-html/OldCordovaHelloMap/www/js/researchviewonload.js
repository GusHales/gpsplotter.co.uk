function onLoad() {
    document.addEventListener("deviceready", onDeciveReady);
    
}

function onDeciveReady() {
    document.addEventListener("backbutton", onBackKeyDown, false);
    viewPost();
}

function viewPost() {  
    
    var researchId = JSON.parse(localStorage.getItem('viewResearchId')) || "";
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/viewresearch.php',
        data: {postViewResearch:researchId},
    success: function(response, status, xhr) {
        var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
        var responseArray = responseClean.split("|");
        responseArray.pop();
        localStorage.setItem('exportDataArray', JSON.stringify(responseArray));
        var i,temparray,chunk = 6,
            j = 0;
        for (i=0; i < responseArray.length; i+=chunk) {
            temparray = responseArray.slice(i,i+chunk);
        $('#addr'+j).html(" <td><h4 name='"+temparray[0]+"' id='researchName'>"+temparray[0]+"</h4></td><td><h4 name='researchName' id='researchName'>"+temparray[1]+"</h4></td><td><h4 name='researchName' id='researchName'>"+temparray[2]+"</h4></td><td><h4 name='researchName' id='researchName'>"+temparray[3]+"</h4></td><td><h4 name='researchName' id='researchName'>"+temparray[4]+"</h3></td><td><h4 name='researchName' id='researchName'>"+temparray[5]+"</h4></td>");
        $('#tab_logic').append('<tr id="addr'+(j+1)+'"></tr>');     
            j++;
        }
    },
    error: function(xhr, status, error) {
        console.log(xhr);
        console.log(status);
        console.log(error);
    }});
    return false; 
}

setTimeout(function(){
    document.getElementById("pageone").style.visibility = "visible";
}, 100);

// Handle the back button
//
function onBackKeyDown() {
    location.href = "private.html";
}

