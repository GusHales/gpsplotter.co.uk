function onLoad() {
    document.addEventListener("deviceready", onDeciveReady);   
}

function onDeciveReady() {
//window.onload = function() {


    var userName = JSON.parse(localStorage.getItem('username')) || "";
    $("#userName").text(userName);
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    console.log(userId);
    privatePost();
    document.addEventListener("backbutton", onBackKeyDown, false);
}

// Handle the back button
//
function onBackKeyDown() {
    location.href = "private.html";
}

function privatePost() {     
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/getResearch.php',
        data: {postUserId:userId},
    success: function(response, status, xhr) {
        var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
        var responseArray = responseClean.split("-");
        var researchNames = [];
        var researchIds = [];
        responseArray.pop();
        console.log(responseArray);
        console.log(responseArray.length);
        for(var i = 0; i < responseArray.length;i++){
            researchIds.push(responseArray[i]);
            researchNames.push(responseArray[i+=1]);

        }
        localStorage.setItem('researchNamesArray', JSON.stringify(researchNames));
        localStorage.setItem('researchIdsArray', JSON.stringify(researchIds));
    },
    error: function(xhr, status, error) {
        console.log("here3");
        console.log(xhr);
        console.log(status);
        console.log(error);
    }});
    return false; 
}

function populateResearch() {
    var researchNames = JSON.parse(localStorage.getItem('researchNamesArray')) || [];
    var researchIds = JSON.parse(localStorage.getItem('researchIdsArray')) || [];
     console.log("here");
    console.log(researchNames , researchIds);
    for(var j = 0; j < researchNames.length; j++) {
        var i=0;

        $('#addr'+j).html("<td id='optionsCell'><h3 name='"+researchNames[j]+"' id='"+researchIds[j]+"'>"+researchNames[j]+"</h3> </td> <td id='optionsCell'><fieldset class='ui-grid-a'><div id='runViewButtons' class='ui-block-a'><button value='"+researchNames[j]+"' id='"+researchIds[j]+"'  onclick='handleView(this.id, this.value)' class='panel-content-btn' data-icon='arrow-r' data-inline='true'>View</button></div><div id='runViewButtons' class='ui-block-b'><button id='"+researchIds[j]+"' onclick='handleRun(this.id)' class='panel-content-btn' data-icon='arrow-r' data-inline='true'>Run</button></div></fieldset></td>");
        $('#tab_logic').append('<tr id="addr'+(j+1)+'"></tr>');
    } 
}

populateResearch();

setTimeout(function(){
    document.getElementById("pageone").style.visibility = "visible";
}, 100);