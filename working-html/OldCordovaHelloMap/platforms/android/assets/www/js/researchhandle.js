var b1 = $("#btn1")[0], b2 = $("#btn2")[0], b3 = $("#btn3")[0],
    b4 = $("#btn4")[0], b5 = $("#btn5")[0], b6 = $("#btn6")[0];

//concept keyboard action
$([b1, b2, b3, b4, b5, b6]).click(function () {
    var lat = JSON.parse(localStorage.getItem('lat'));
    var long = JSON.parse(localStorage.getItem('long'));
    var acc = JSON.parse(localStorage.getItem('acc'));
//    var speed = JSON.parse(localStorage.getItem('speed')) || [];
    var date = new Date();
    var time = date.getTime();
    var newDate = new Date(time);
    var stringDate = String(newDate);
    var connectionStatus = checkConnection();
    var eventId = generateEventId();

    document.getElementById('latText').value = lat;
    document.getElementById('longText').value = long;
    document.getElementById('timeText').value = stringDate;
    document.getElementById('accText').value = acc;
    document.getElementById('eventId').value = eventId;
//    document.getElementById('speedText').value = speed;
    
    
    if(connectionStatus != 'NotConnected') {
        document.getElementById("result").innerHTML = "Data saved";
        $.getScript('js/post.js', function() {
            post();
        });
        $.getScript('js/cacher.js', function() {
            cacherPost();
        });
    } else {
        //call cacher script
        document.getElementById("result").innerHTML = "Data has been cached";
        $.getScript('js/cacher.js', function() {
            dataCacher();
        });
    }

    var reply = document.querySelector('#result').innerHTML.trim(" ");
});



//check the current conncetion status
function checkConnection() {
    var networkState = navigator.connection.type;

    var states = {};
    states[Connection.UNKNOWN]  = 'Connected';
    states[Connection.ETHERNET] = 'Connected';
    states[Connection.WIFI]     = 'Connected';
    states[Connection.CELL_2G]  = 'Connected';
    states[Connection.CELL_3G]  = 'Connected';
    states[Connection.CELL_4G]  = 'Connected';
    states[Connection.CELL]     = 'Connected';
    states[Connection.NONE]     = 'NotConnected';

    return states[networkState];
}

// Generate event id
function generateEventId() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}