function post() {    
    var latText = $('#latText').val();
    var longText = $('#longText').val();
    var timeText = $('#timeText').val();
    var accText = $('#accText').val();
//    var speedText = $('#speedText').val();
    var researchId = $('#researchId').val();
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    var btnText = $('#btnText').val();
    var resName = JSON.parse(localStorage.getItem('researchName')) || [];
    var btnDesc = $('#btnDesc').val();
    var eventId = $('#eventId').val();
       
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: 'http://gpsplotter.co.uk/php/server.php',
        data: {postUserId:userId,postResearchId:researchId,postLatText:latText,postLongText:longText,postTimeText:timeText,postAccText:accText,postBtnText:btnText,
                             postResName:resName[0], postBtnDesc:btnDesc, postEventId:eventId},
    success: function(response) {
        console.log(response);
        //reply to the result div tag
    },
    error: function(xhr, status, error) {
       console.log(xhr);
       console.log(status);
       console.log(error);
    }});
    return false; 
}
