//once new research is pressed the data will be cleared.
$('#newResearch').click(function(){
    location.href= 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function(){
    location.href= 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
});

$('#account').click(function(){
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    console.log(userId);
    location.href = "private.html";
});

$('#export').click(function(){
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
});

// Generates research id
function generateResearchId(){
    return Math.random().toString(36).substr(2, 9);
}


function gotFS(fileSystem) {
   var fileName = JSON.parse(localStorage.getItem('viewResearchName')) + "-" + JSON.parse(localStorage.getItem('viewResearchId')) + ".txt";
    console.log(fileName);
    
    fileSystem.root.getFile(fileName, {create: true, exclusive: false}, gotFileEntry, fail);
}

function gotFileEntry(fileEntry) {
    fileEntry.createWriter(gotFileWriter, fail);
}

function gotFileWriter(writer) {
       var fileName = JSON.parse(localStorage.getItem('viewResearchName')) + "-" + JSON.parse(localStorage.getItem('viewResearchId'));
    alert("The file "+fileName+".txt has been written to, /storage/emulated/");
    
    var exportDataArray = JSON.parse(localStorage.getItem('exportDataArray')) || [],
    researchName = "Research name = " + JSON.parse(localStorage.getItem('viewResearchName')) || "",
    headings = "Latitude - Longitude - accuracy - time - button_name - button_long_name",
    i,temparray,chunk = 6,
    j = 0, writerString = researchName + "\n" + headings;
    
    for (i=0; i < exportDataArray.length; i+=chunk) {
    temparray = exportDataArray.slice(i,i+chunk);
     writerString = writerString + "\n" + temparray;
    
    }
        writer.write(writerString);
}

function fail(error) {
    alert("error");
    console.log(error.code);
}