$("#createResearch").click(function () {
    var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [];
    var oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [];
    var oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [];
    var finalButtonArray = [];
    var finalButtonDescArray = [];
    
    postNewResearch();
    
    setTimeout(function() {
        if (oldButtons.length >= 1) {
        location.href = 'research.html';
        for (var i = 0; i < oldButtons.length; i++) {
            finalButtonArray.push(oldButtons[i]);
            finalButtonArray.push(oldButtonsName[i]);
            finalButtonDescArray.push(oldButtons[i]);
            finalButtonDescArray.push(oldButtonsDesc[i]);
        }
        localStorage.setItem('finalButtonArray', JSON.stringify(finalButtonArray));
        localStorage.setItem('finalButtonDescArray', JSON.stringify(finalButtonDescArray));
    } else {
        document.getElementById("errorPane").style.visibility = "visible";
        document.getElementById("errorMsg").style.visibility = "visible";
        document.querySelector("#errorMsg").innerHTML = "Error: Please add atleast two buttons.";
    }
},3000);
});

//once new research is pressed the data will be cleared.
$('#newResearch').click(function(){
    location.href= 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function(){
    location.href= 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
});

$('#account').click(function(){
    location.href = "private.html";
});

// Generates research id
function generateResearchId(){
    return Math.random().toString(36).substr(2, 9);
}

$(document).on('click', '.input-remove-row', function() { 
    var tr = $(this).closest('tr');
    var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [];
    var oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [];
    var oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [];
    tr.fadeOut(200, function(){
        console.log(tr[0].innerText);
    	tr.remove();
        var remover = tr[0].innerText;
        console.log("test " + tr[0].innerText.substring(0,4));
        for(var i = 0; i < oldButtons.length; i++) {
            if(remover.indexOf(tr[0].innerText.substring(0,4)) > -1) {
                if(oldButtons[i] === tr[0].innerText.substring(0,4)) {
                    oldButtons.splice(i , 1);
                    oldButtonsName.splice(i , 1);
                    oldButtonsDesc.splice(i , 1);
                }
            }
            localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
            localStorage.setItem('buttonsNameArray', JSON.stringify(oldButtonsName));
            localStorage.setItem('buttonsDescArray', JSON.stringify(oldButtonsDesc));
        }
	});
});

$(function() {
    $("#researchName").keydown(function(e) {
        if(e.keyCode==13) {
            document.getElementById("buttons").focus();
        }
    });
     $("#buttonName").keydown(function(e){
        if(e.keyCode==13) {
            document.getElementById("buttonDesc").focus();
        }
    });
    $("#buttonDesc").keydown(function(e){
        if(e.keyCode==13) {
            document.getElementById("addButton").focus();
        }
    });
});

$("#searchclear").click(function(){
    document.getElementById('researchName').readOnly = false;
    $("#researchName").val('');
});

$(function() {
    $('.preview-add-button').click(function() {
        var formData = {};
        
        var researchName = JSON.parse(localStorage.getItem('researchName')) || [];
        var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [];
        var oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [];
        var oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [];
    
        if($.inArray($('.research-form #buttons option:selected').text(), oldButtons) != -1) {
            document.getElementById("errorPane").style.visibility = "visible";
            document.getElementById("errorMsg").style.visibility = "visible";
            document.querySelector("#errorMsg").innerHTML = "Error: " + $('.research-form #buttons option:selected').text()  + " button already added.";
        } else {
            if($('.research-form #buttons option:selected').text() != "Select") {
                document.getElementById("errorPane").style.visibility = "hidden";
                document.getElementById("errorMsg").style.visibility = "hidden";
                if($('.research-form input[name="buttonName"]').val() != "") {
                    formData["buttons"] = $('.research-form #buttons option:selected').text();
                    formData["buttonName"] = $('.research-form input[name="buttonName"]').val();
                    formData["buttonDesc"] = $('.research-form input[name="buttonDesc"]').val();
                    formData["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';
                    oldButtonsName.push($('.research-form input[name="buttonName"]').val());    
                    oldButtons.push($('.research-form #buttons option:selected').text());
                    oldButtonsDesc.push($('.research-form input[name="buttonDesc"]').val());
                    researchName = [];
                    researchName.push($('.research-form input[name="researchName"]').val());
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.querySelector("#errorMsg").innerHTML = "Error: * Please enter required fields.";
                }
                if(researchName != "") {
                    document.getElementById("researchName").readOnly = true;
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.querySelector("#errorMsg").innerHTML = "Error: * Please enter required fields.";
                }
            } else {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: * Please enter required fields.";
            }
        } 

        var row = $('<tr></tr>');
        $.each(formData, function( type, value ) {
            $('<td class="input-'+type+'"></td>').html(value).appendTo(row);
        });
        $('.preview-table > tbody:last').append(row); 
        
        localStorage.setItem('researchName', JSON.stringify(researchName));
        localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
        localStorage.setItem('buttonsNameArray', JSON.stringify(oldButtonsName));
        localStorage.setItem('buttonsDescArray', JSON.stringify(oldButtonsDesc));
        
        

    });
});

function postNewResearch() {     
    var researchId = localStorage['researchId'];
    var researchName = JSON.parse(localStorage.getItem('researchName')) || [];
    var buttonsNumber = localStorage.getItem('buttonsArray') || [];
    var buttonsCreated = localStorage.getItem('buttonsNameArray') || [];
    var buttonsCreatedLongName = localStorage.getItem('buttonsDescArray') || [];

    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/setresearch.php',
        data: {postSetResearchId:researchId, postSetResearchName:researchName.toString(), postSetButtonsNumber:buttonsNumber, postSetButtonsCreated:buttonsCreated, postSetButtonsCreatedLongName:buttonsCreatedLongName},
    success: function(response, status, xhr) {
        var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
    },
    error: function(xhr, status, error) {
        console.log("here3");
        console.log(xhr);
        console.log(status);
        console.log(error);
    }});
    return false; 
}

