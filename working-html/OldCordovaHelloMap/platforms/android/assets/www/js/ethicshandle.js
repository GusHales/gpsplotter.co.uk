$("#continue").click(function () {
    var terms = document.getElementById("checkbox-1").checked;
    var researchId = generateResearchId();
    if(terms === true) {
        localStorage.setItem('buttonsArray', '[]');
        localStorage.setItem('buttonsNameArray', '[]');
        localStorage.setItem('buttonsDescArray', '[]');
        localStorage.setItem('researchName', '[]');
        localStorage.setItem('researchId', researchId);
        localStorage.setItem('researchedDataArray', '[]');
        
        location.href = 'newresearch.html';
    } else {
        location.href= 'index.html';
    }
});

//once home is pressed the data will be cleared.
$('#homePage').click(function(){
    location.href = 'index.html';
});


// Generates research id
function generateResearchId(){
    return Math.random().toString(36).substr(2, 9);
}