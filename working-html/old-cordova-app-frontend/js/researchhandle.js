//check the current conncetion status
function checkConnection() {
    'use strict';
    var networkState = navigator.connection.type,
        states = {};
    states[Connection.UNKNOWN]  = 'Connected';
    states[Connection.ETHERNET] = 'Connected';
    states[Connection.WIFI]     = 'Connected';
    states[Connection.CELL_2G]  = 'Connected';
    states[Connection.CELL_3G]  = 'Connected';
    states[Connection.CELL_4G]  = 'Connected';
    states[Connection.CELL]     = 'Connected';
    states[Connection.NONE]     = 'NotConnected';

    return states[networkState];
}

// Generate event id
function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function post() {
    'use strict';
    var latText = $('#latText').val(),
        longText = $('#longText').val(),
        timeText = $('#timeText').val(),
        accText = $('#accText').val(),
        researchId = $('#researchId').val(),
        userId = JSON.parse(localStorage.getItem('userId')) || "",
        btnText = $('#btnText').val(),
        resName = JSON.parse(localStorage.getItem('researchName')) || [],
        btnDesc = $('#btnDesc').val(),
        eventId = $('#eventId').val();
       
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: 'http://gpsplotter.co.uk/php/server.php',
        data: {postUserId: userId, postResearchId: researchId, postLatText: latText, postLongText: longText, postTimeText: timeText, postAccText: accText, postBtnText: btnText,
                             postResName: resName[0], postBtnDesc: btnDesc, postEventId: eventId},
        success: function (response) {
            console.log(response);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}


var b1 = $("#btn1")[0], b2 = $("#btn2")[0], b3 = $("#btn3")[0],
    b4 = $("#btn4")[0], b5 = $("#btn5")[0], b6 = $("#btn6")[0];

//concept keyboard action
$([b1, b2, b3, b4, b5, b6]).click(function () {
    'use strict';
    var lat = JSON.parse(localStorage.getItem('lat')),
        long = JSON.parse(localStorage.getItem('long')),
        acc = JSON.parse(localStorage.getItem('acc')),
        date = new Date(),
        time = date.getTime(),
        newDate = new Date(time),
        stringDate = String(newDate),
        connectionStatus = checkConnection(),
        eventId = generateEventId(),
        reply;

    document.getElementById('latText').value = lat;
    document.getElementById('longText').value = long;
    document.getElementById('timeText').value = stringDate;
    document.getElementById('accText').value = acc;
    document.getElementById('eventId').value = eventId;
     
    if (connectionStatus !== 'NotConnected') {
        document.getElementById("result").innerHTML = "Data saved";
        post();
        $.getScript('js/cacher.js', function () {
            cacherPost();
        });
    } else {
        //call cacher script
        document.getElementById("result").innerHTML = "Data has been cached";
        $.getScript('js/cacher.js', function () {
            dataCacher();
        });
    }
    reply = document.querySelector('#result').innerHTML.trim(" ");
});