function postLogin() {
    'use strict';
    var username = $('#username').val(),
        password = $('#password').val(),
        responseClean,
        responseArray;
    
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/login.php',
        data: {postUsername: username, postPassword: password},
    
        success: function (response, status, xhr) {
        
            responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
        
            localStorage.setItem('loginReply', JSON.stringify(responseClean));
            localStorage.setItem('username', JSON.stringify(username));

            if (responseClean.indexOf("error") > -1) {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Login failed.";
            } else if (responseClean.indexOf("private.html") > -1) {
                responseArray = responseClean.split(" ");
                localStorage.setItem('userId', JSON.stringify(responseArray[1]));
                document.getElementById("errorPane").style.visibility = "hidden";
                document.getElementById("errorMsg").style.visibility = "hidden";
                location.href = responseArray[0];
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

//once home is pressed the data will be cleared.
$('#homePage').click(function () {
    'use strict';
    location.href = 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('researchedDataArray', '[]');
});

$("#loginButton").click(function () {
    'use strict';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '[]');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
//    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
    postLogin();
});

$("#register").click(function () {
    'use strict';
    location.href = "register.html";
});

$(function () {
    'use strict';
    $("#username").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("password").focus();
        }
    });
     
    $("#password").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("loginButton").focus();
        }
    });
});


