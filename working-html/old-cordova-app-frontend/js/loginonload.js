function onBackKeyDown() {
    'use strict';
    location.href = "index.html";
}

function onDeciveReady() {
    'use strict';
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onLoad() {
    'use strict';
    document.addEventListener("deviceready", onDeciveReady);
}

setTimeout(function () {
    'use strict';
    document.getElementById("pageone").style.visibility = "visible";
}, 100);