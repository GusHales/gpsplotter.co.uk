function moreInfo() {
    'use strict';
    $("#moreInfo").click(function () {
        location.href = 'test.html';
    });
}

//once new research is pressed the data will be cleared.
function homePage() {
    'use strict';
    //once home is pressed the data will be cleared.
    $('#homePage').click(function () {
        location.href = 'index.html';
    });
}


function login() {
    'use strict';
    $("#login").click(function () {
        location.href = 'login.html';
    });
}


homePage();
moreInfo();
login();