// Generates research id
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}


//once new research is pressed the data will be cleared.
$('#newResearch').click(function () {
    'use strict';
    location.href = 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function () {
    'use strict';
    location.href = 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
});

$('#account').click(function () {
    'use strict';
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    location.href = "private.html";
});

function gotFileWriter(writer) {
    'use strict';
    var fileName = JSON.parse(localStorage.getItem('viewResearchName')) + "-" + JSON.parse(localStorage.getItem('viewResearchId')),
        exportDataArray = JSON.parse(localStorage.getItem('exportDataArray')) || [],
        researchName = "Research name = " + JSON.parse(localStorage.getItem('viewResearchName')) || "",
        headings = "Latitude - Longitude - accuracy - time - button_name - button_long_name",
        i,
        temparray,
        chunk = 6,
        j = 0,
        writerString = researchName + "\n" + headings;
    
    alert("The file " + fileName + ".txt has been written to, /storage/emulated/");
    
    for (i = 0; i < exportDataArray.length; i += chunk) {
        temparray = exportDataArray.slice(i, i + chunk);
        writerString = writerString + "\n" + temparray;
    }
    writer.write(writerString);
}

function fail(error) {
    'use strict';
    alert("Error file did not write!");
    console.log(error.code);
}

function gotFileEntry(fileEntry) {
    'use strict';
    fileEntry.createWriter(gotFileWriter, fail);
}

function gotFS(fileSystem) {
    'use strict';
    var fileName = JSON.parse(localStorage.getItem('viewResearchName')) + "-" + JSON.parse(localStorage.getItem('viewResearchId')) + ".txt";
    fileSystem.root.getFile(fileName, {create: true, exclusive: false},  gotFileEntry, fail);
}

$('#export').click(function () {
    'use strict';
    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
});