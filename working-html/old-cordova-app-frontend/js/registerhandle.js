function postRegister() {
    'use strict';
    var username = $('#username').val(),
        email = $('#email').val(),
        password = $('#password').val(),
        userId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16|0, v = c === 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/register.php',
        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
        success: function (response, status, xhr) {
            var responseClean = response.replace(/(?:\r\n|\s|\r|\n)/g, '');
            localStorage.setItem('loginReply', JSON.stringify(responseClean));

            if (responseClean === "enterUsername") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Please enter a username. ";
            } else if (responseClean === "enterPassword") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Please enter a password. ";
            } else if (responseClean === "invalidEmail") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Please enter a valid email address. ";
            } else if (responseClean === "usernameInUse") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Username already in use. ";
            } else if (responseClean === "emailInUse") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Email address already in use. ";
            } else if (responseClean === "login.html") {
                location.href = responseClean;
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

//once new research is pressed the data will be cleared.
$('#login').click(function () {
    'use strict';
    location.href = 'login.html';
});

//once new research is pressed the data will be cleared.
$('#homePage').click(function () {
    'use strict';
    location.href = 'index.html';
});

$('#registerButton').click(function () {
    'use strict';
    postRegister();
});

$(function () {
    'use strict';
    $("#username").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("email").focus();
        }
    });
    
    $("#email").change(function () {
        document.getElementById("password").focus();
    });
    
    $("#password").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("registerButton").focus();
        }
    });
    
    $("#buttonDesc").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("addButton").focus();
        }
    });
});