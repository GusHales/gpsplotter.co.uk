function onMapInit(map) {
    'use strict';
    map.clear();

    var onSuccess = function (location) {
        
        var date = new Date(location.time);
        var msg = ["Current your location:\n",
            "latitude:" + location.latLng.lat,
            "longitude:" + location.latLng.lng,
            "speed:" + location.speed,
            "accuracy:" + location.accuracy,
            "time:" + date,
            "realTime:" + location.elapsedRealtimeNanos,      
            "bearing:" + location.bearing,
            "provider:" + location.provider].join("\n");
    
        localStorage.setItem('lat', JSON.stringify(location.latLng.lat));
        localStorage.setItem('long', JSON.stringify(location.latLng.lng));
        localStorage.setItem('time', JSON.stringify(location.time));
        localStorage.setItem('acc', JSON.stringify(location.accuracy));
        localStorage.setItem('speed', JSON.stringify(location.speed));
        
        map.addMarker({
            'position': location.latLng,
            'title': msg,
            disableAutoPan: true
            
        }, function (marker) {
            marker.showInfoWindow();
            marker.setOpacity(1.0);
            setTimeout(function () {
                marker.setOpacity(0.25);
            }, 5000);
            marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function () {
                map.addCircle({
                    'center': location.latLng,
                    'radius': location.accuracy,
                    'strokeColor' : '#619fdd',
                    'strokeWidth': 2,
                    'fillColor' : '#92b7db'
                    
                }, function (circle) {
                    setTimeout(function () {
                        circle.remove();
                    }, 10000);
                });
            });
        });
    
    
        map.addCircle({
            'center': location.latLng,
            'radius': location.accuracy,
            'strokeColor' : '#619fdd',
            'strokeWidth': 2,
            'fillColor' : '#92b7db'
        }, function (circle) {
            setTimeout(function () {
                circle.remove();
            }, 10000);
        });
      
        map.moveCamera({
            'target': location.latLng,
            'zoom': 17
        });
    },
        onError = function (msg) {
            alert("error: " + msg);
        };
    map.setMyLocationEnabled(true);
    map.getMyLocation({'enableHighAccuracy': true},onSuccess, onError);
    

}



// Generates research id
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

document.addEventListener("deviceready", function () {
    'use strict';
    // Define a div tag with id="map_canvas"
    var mapDiv = document.getElementById("map-canvas"),
        // Initialize the map plugin
        map = plugin.google.maps.Map.getMap(mapDiv),
        r;
    
    onMapInit(map);

    function hideMap() {
        map.setVisible(false);
    }

    function showMap() {
        map.refreshLayout();
        map.setVisible(true);
    }
    showMap();
    
    var b1 = $("#btn1")[0],
        b2 = $("#btn2")[0],
        b3 = $("#btn3")[0],
        b4 = $("#btn4")[0],
        b5 = $("#btn5")[0],
        b6 = $("#btn6")[0];
    
    $([b1, b2, b3, b4, b5, b6]).click(function () {
        alert("buttons pressed");
        map.refreshLayout();
        onMapInit(map);
    });
    
    
    //done button action
    $("#doneButton").click(function () {
        hideMap();
        r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
        if (r === true) {
            $.getScript('js/cacher.js', function () {
                cacherPost();
            });
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '[]');
            localStorage.setItem('researchId', '[]');
            localStorage.setItem('researchedDataArray', '[]');
            setTimeout(function () {location.href = 'private.html'; }, 3000);
        } else {
            showMap();
        }
    });
    
    //once new research is pressed the data will be cleared.
    $('#newResearch').click(function () {
        hideMap();
        r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
        if (r === true) {
            location.href = 'newresearch.html';
            var researchId = generateResearchId();
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '[]');
            localStorage.setItem('researchId', researchId);
            localStorage.setItem('researchedDataArray', '[]');
        } else {
            showMap();
        }
    });

    //once new research is pressed the data will be cleared.
    $('#logout').click(function () {
        hideMap();
        r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
        if (r === true) {
            location.href = 'index.html';
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '[]');
            localStorage.setItem('researchedDataArray', '[]');
            localStorage.setItem('researchId', '[]');
            localStorage.setItem('loginReply', '""');
            localStorage.setItem('username', '""');
            localStorage.setItem('userId', '""');
            localStorage.setItem('researchNamesArray', '[]');
            localStorage.setItem('researchIdsArray', '[]');
        } else {
            showMap();
        }
    });

    $('#account').click(function () {
        hideMap();
        r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
        if (r === true) {
            location.href = "private.html";
        } else {
            showMap();
        }
    });

    $("#myPanel").panel({
        "beforeclose": hideMap,
        "close": showMap,
        "beforeopen": hideMap,
        "open": showMap
    });
});