CREATE TABLE research_created (
	research_id VARCHAR(255) NOT NULL,
	research_name VARCHAR(255) NOT NULL,
	buttons_number VARCHAR(64) NOT NULL,
	buttons_created VARCHAR(255) NOT NULL,
	buttons_created_long_name VARCHAR(255),
	PRIMARY KEY (research_id)
);
