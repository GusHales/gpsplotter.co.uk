CREATE TABLE research_data (
	research_data_id VARCHAR(255) NOT NULL,
	latitude VARCHAR(255),
	longitude VARCHAR(255),
	accuracy VARCHAR(255),
	heading VARCHAR(255),
	speed VARCHAR(255),
	time VARCHAR(255),
	time_in_mill VARCHAR(255),
	button_name VARCHAR(255),
	button_long_name VARCHAR(255),
	PRIMARY KEY (research_data_id)
);