CREATE TABLE users (
	user_id VARCHAR(255) NOT NULL,
	username VARCHAR(255) NOT NULL,
	password CHAR(65) NOT NULL,
	salt CHAR(16) NOT NULL,
	email VARCHAR(255) NOT NULL,
	PRIMARY KEY (user_id),
	UNIQUE KEY username (username),
	UNIQUE KEY email (email)
);
