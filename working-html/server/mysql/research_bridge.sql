CREATE TABLE research_created (
	research_id VARCHAR(255) NOT NULL,
	research_data_id VARCHAR(255) NOT NULL,
	user_id VARCHAR(255) NOT NULL,
	PRIMARY KEY (research_id, research_data_id, user_id)
);
