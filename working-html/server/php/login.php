<?php
/**
 * Handle users login.
 *
 * Once the user inputs data and posts it to the server, this
 * will handle that post.
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

//Add the conncetion.php file so that we are able to run our queries.
require_once("connection.php"); 

/**
 * Check that the ajax post is not empty.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 *
 */  
function loginPostCheck($db, $date, $errorLogLocation) {
    if(!empty($_POST)) {
        //Users login status, initialized.
        $loginOk = false;
        $row = queryDbForLogin($db, $date, $errorLogLocation);
        $loginOk = passwordChecker($row, $date, $errorLogLocation);
    loginChecker($loginOk, $row, $date, $errorLogLocation);
    }
}

loginPostCheck($db, $date, $errorLogLocation);


/**
 * Query the database to check if the username exists.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 *
 */  
function queryDbForLogin($db, $date, $errorLogLocation) { 
    //First query which will select everything from users where the username is euqals to the
    //post username.
    $query = "SELECT * FROM users WHERE username = :username"; 
 
    //Parameter values.
    $query_params = array( 
        ':username' => $_POST['postUsername'] 
    ); 

    //Try/catch which will catch any errors and log the information to the servers log file.  
    try { 
        //Run query against db.
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } catch(PDOException $ex) { 
    //Kill the conncetion with an error message but also a server log. 
        error_log($date . " SQL-ERROR-101: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
        die("Failed to run query: " . $ex->getMessage()); 
    } 
    //Retrive the requested information. If results exists then continue.
    $row = $stmt->fetch();
    return $row;
}
	
/**
 * Check that the users password matches the hashed one from the db.
 *
 * @param array $row Row results from the query
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 *
 */  	                       
function passwordChecker($row, $date, $errorLogLocation) {
    if($row) { 
        //Take the users password apply the appropriate salt and then by hasing the submitted password
        //it's then compared agains the hashed value which is stored in the database.
        $check_password = hash('sha256', $_POST['postPassword'] . $row['salt']); 
        for($round = 0; $round < 65536; $round++) { 
            $check_password = hash('sha256', $check_password . $row['salt']); 
        } 
 
        if($check_password === $row['password']) { 
            //Change user status to true;
            return true;
        } else {
            error_log($date . " USER-ERROR-301: " . $row['username'] . " has entered wrong password.\n", 3, $errorLogLocation);
            echo 'error';
        }
    } else {
        error_log($date . " SQL-ERROR-102: 0 results found in users table.\n", 3, $errorLogLocation);
        echo 'error';
    }
} 

/**
 * Check to see if the login is ok then continue to their private section.
 *
 * @param boolean $loginOk Boolean to indicate if the password is ok
 * @param array $row Row results from the query
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 *
 */  
function loginChecker($loginOk, $row, $date, $errorLogLocation) {
    if($loginOk) {

    error_log($date . " USER-LOG-201: User " . $row['username'] . " has logged in.\n", 3, $errorLogLocation);

    echo "private.html ", $row['user_id'];
    } else { 
        error_log($date . " USER-ERROR-302: Login failed.\n", 3, $errorLogLocation);
        echo 'error';
    }
}

?> 
