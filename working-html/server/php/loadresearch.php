<?php
/**
 * Load a research for a user.
 *
 * The function below will handle the event of the run 
 * button press
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
//Add the conncetion.php file so that we are able to run our queries.
require("connection.php"); 

/**
 * Handle the run button press event.
 *
 * If the user desides to press the run button this function will,
 * load by querying the database and returning the appropriate
 * research.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 */  
function runButtonPressed($db, $date, $errorLogLocation) {
    //Check that the ajax post is not empty.
    if(!empty($_POST)) { 
        //First query which will get everything from research_created table and this
        //will be done by using the research_id.
        $query = "SELECT * FROM research_created WHERE research_id = :research_id"; 

        //Parameter values.
        $query_params = array( 
            ':research_id' => $_POST['postRequestResearchId'] 
        ); 

        //Try/catch which will catch any errors and log the information to the servers log file.   
        try { 
            //Run query against db.
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) { 
            //Kill the conncetion with an error message but also a server log. 
            error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
            die("Failed to run query: " . $ex->getMessage()); 
        }
        
        //Retrive the requested information. If results exists then continue.
        $row = $stmt->fetch(); 
        if($row) { 
        	do {
                    echo $row["research_name"] . "-" . $row["buttons_number"] . "-" . $row["buttons_created"] . "-" . $row["buttons_created_long_name"];  
        	} while($row = $stmt->fetch());
        } else {
            error_log($date . " SQL-ERROR-103: O results from research_created table\n", 3, $errorLogLocation);
            echo "O results from research_created table";
        }
    }
}
runButtonPressed($db, $date, $errorLogLocation);
?>
