<?php
/**
 * Populate the research_data database.
 *
 * Once the research is created and the researcher is
 * ready to gather data. The function below will allow
 * the gathered data to be stored
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

//Add the conncetion.php file so that we are able to run our queries.
require("connection.php"); 

/**
 * Populates the research_data table with all the gathered data.
 *
 * Once the user has started pressing the concept keyboard keys, that
 * data will be sent to here where it will be stored with this function.
 * The catching system also call this function which allows it to perform
 * the same action when necessary.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 */ 
function conceptKeyboardKeyPressed($db, $date, $errorLogLocation) {
//Check that the ajax post is not empty.
if(!empty($_POST)) { 
    
    //First query which will insert all the data gathered by the user in the research_data table. 
    $query = "INSERT INTO research_data (research_data_id, latitude, longitude, accuracy, heading, speed, time, time_in_mill, button_name, button_long_name)
    VALUES (:research_data_id, :latitude, :longitude, :accuracy, :heading, :speed, :time, :time_in_mill, :button_name, :button_long_name)"; 

    //Parameter values.
    $query_params = array( 
        ':research_data_id' => $_POST['postEventId'],
        ':latitude' => $_POST['postLatText'],
        ':longitude' => $_POST['postLongText'],
        ':accuracy' => $_POST['postAccText'],
        ':heading' => $_POST['postHeadingText'],
        ':speed' => $_POST['postSpeedText'],
        ':time' => $_POST['postTimeText'],
        ':time_in_mill' => $_POST['postMillDateText'],
        ':button_name' => $_POST['postBtnText'],
        ':button_long_name' => $_POST['postBtnDesc']
    ); 

    //Try/catch which will catch any errors and log the information to the servers log file.  
    try { 
        //Run query against db.
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } 
    catch(PDOException $ex) { 
        //Kill the conncetion with an error message but also a server log. 
        error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
        die("Failed to run query: " . $ex->getMessage()); 
    } 
    
    //Second query which will insert data to the research_bridge table.
    $query2 = "INSERT INTO research_bridge (research_id, research_data_id, user_id)
    VALUES (:research_id, :research_data_id, :user_id)"; 


    //Parameter values.
    $query_params2 = array( 
        ':research_id' => $_POST['postResearchId'],
        ':research_data_id' => $_POST['postEventId'],
        ':user_id' => $_POST['postUserId'],
    ); 

    try { 
        //Run query against db.
        $stmt2 = $db->prepare($query2); 
        $result2 = $stmt2->execute($query_params2); 
    } 
    catch(PDOException $ex2) { 
        //Kill the conncetion with an error message but also a server log. 
        error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex2->getMessage(), 3, $errorLogLocation);
        die("Failed to run query: " . $ex2->getMessage()); 
    } 
    
    if($result === TRUE && $result2 === TRUE) {
        echo 'sucess';
        error_log($date . " USER-LOG-204: Tables populated, research " . $_POST['postResearchId'] . " by user " . $_POST['postUserId'] . "\n", 3, $errorLogLocation);
    } else {
        echo 'insert error';
        error_log($date . " SQL-ERROR-104: Error inserting into tables\n", 3, $errorLogLocation);
    }
}
}
conceptKeyboardKeyPressed($db, $date, $errorLogLocation);
?>
