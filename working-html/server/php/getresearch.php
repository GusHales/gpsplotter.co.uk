<?php
/**
 * Populate the private.html table.
 *
 * Once the user has logged in they will be presented with a table. If they
 * have created any research the function below will load them.
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
//Add the conncetion.php file so that we are able to run our queries.
require("connection.php"); 

/**
 * Populates the private.html table with the created research
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 */ 
function getResearch($db, $date, $errorLogLocation) {
//Check that the ajax post is not empty.
if(!empty($_POST)) { 
    //First query which will get the research used the research_bridge table, this is done by
    //using the users_id.
    $query = "SELECT research_id FROM research_bridge WHERE user_id = :user_id GROUP BY research_id"; 

    //Parameter values.
    $query_params = array( 
        ':user_id' => $_POST['postUserId'] 
    ); 

    //Try/catch which will catch any errors and log the information to the servers log file.
    try { 
        //Run query against db.
        $stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
    } 
    catch(PDOException $ex) { 
        //Kill the conncetion with an error message but also a server log.
        error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
        die("Failed to run query: " . $ex->getMessage()); 
    }
    
    //Retrive the requested information. If results exists then continue.
    $row = $stmt->fetch(); 
    if($row) {
        //Do while loop to look through all of the results. 
	    do {
            $resId = $row["research_id"];
            echo $resId . "|";
            $query2 = "SELECT research_name FROM research_created WHERE research_id = :research_id";

            //Parameter values.
            $query_params2 = array( 
                ':research_id' => $resId 
            ); 
            try { 
                //Run query against db.
                $stmt2 = $db->prepare($query2); 
                $result2 = $stmt2->execute($query_params2); 
            } 
            catch(PDOException $ex2) { 
                //Kill the conncetion with an error message but also a server log.
                error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex2->getMessage(), 3, $errorLogLocation);
                die("Failed to run query: " . $ex2->getMessage()); 
            }

            //Retrive the requested information. If results exists then continue.
            $row2 = $stmt2->fetch(); 
            if($row2) {
                //Do while loop to look through all of the results.
        		do {
        			echo $row2["research_name"] . "|";
        		}
        		 while($row2 = $stmt2->fetch());
            } else {
                error_log($date . " SQL-ERROR-103: O results from research_created table\n", 3, $errorLogLocation);
                //echo "O results from research_created table";
            }
        } while($row = $stmt->fetch());
    } else {
        error_log($date . " SQL-ERROR-103: O results from research_bridge table\n", 3, $errorLogLocation);
        echo "O results from research_bridge table";
    }
}
}
getResearch($db, $date, $errorLogLocation);
?>

