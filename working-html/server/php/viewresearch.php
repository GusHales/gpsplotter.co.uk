<?php
/**
 * Populate table in view research.
 *
 * The function below will populate the appropriate table in
 * the view research screen.s
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

//Add the conncetion.php file so that we are able to run our queries.
require("connection.php"); 

/**
 * Populate the view research table once the user clicks the view button
 *
 * Once the user is redirected to viewresearch.html the table must be populated
 * with the correct data. The function bellow queries the database with the post
 * research_id and brings back all of the data that belongs to that id.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 */ 
function pupulateTable($db, $date, $errorLogLocation) {
    //Check that the ajax post is not empty.
    if(!empty($_POST)) { 

        //First query select research_data_if from research_bridge, this query will provide the research data.
        $query = "SELECT research_data_id FROM research_bridge WHERE research_id = :research_id"; 

        //Parameter values.
        $query_params = array( 
            ':research_id' => $_POST['postViewResearch'] 
        );     
        
        //Try/catch which will catch any errors and log the information to the servers log file.  
        try { 
            //Run query against db.
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) { 
            //Kill the conncetion with an error message but also a server log. 
            error_log("SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
            die("Failed to run query: " . $ex->getMessage()); 
        }
        
        //Retrive the requested information. If results exists then continue.
        $row = $stmt->fetch(); 
        if($row) { 
    	    do {
                $researchDataId = $row["research_data_id"];
                
                //Second query which will used the research_data_id from the first query to retreave all of that research data.
                $query2 = "SELECT * FROM research_data WHERE research_data_id = :research_data_id"; 
                
                //Parameter values.
                $query_params2 = array( 
                    ':research_data_id' => $researchDataId 
                );     

                try { 
                    //Run query against db.
                    $stmt2 = $db->prepare($query2); 
                    $result2 = $stmt2->execute($query_params2); 
                } 
                catch(PDOException $ex2) { 
                    //Kill the conncetion with an error message but also a server log. 
                    error_log("SQL-ERROR-102: Failed to run query: \n" . $ex2->getMessage(), 3, $errorLogLocation);
                    die("Failed to run query: " . $ex2->getMessage()); 
                }

                //Retrive the requested information. If results exists then continue.
                $row2 = $stmt2->fetch(); 
        	    if($row2) {
                        do {
                            //while results print them to the client.
                           echo $row2["latitude"] . "|" . $row2["longitude"] . "|" . $row2["accuracy"] . "|" . $row2["heading"] . "|" . $row2["speed"] . "|" . $row2["time"] . "|" . $row2["time_in_mill"] . "|" . $row2["button_name"] . "|" . $row2["button_long_name"] . "|";  
                        } while($row2 = $stmt2->fetch());
                    } else {
                        error_log("SQL-ERROR-103: O results from research_data table\n", 3, $errorLogLocation);
                        echo "O results from research_data table";
                    }
                } while ($row = $stmt->fetch());
        } else {
            error_log("SQL-ERROR-103: O results from research_bridge table\n", 3, $errorLogLocation);
            echo "O results from research_bridge table";
        }
    }
}
pupulateTable($db, $date, $errorLogLocation);
?>

