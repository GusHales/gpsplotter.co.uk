<?php 
/**
 * Handle when the user presses the run button.
 *
 * Once the user has created a research and they require to run it again,
 * they can choose to press the run button in priovate.html then this will
 * be called via an ajax post which will then insert a message in the database
 * which will say Run indicating the user that they ran the research at that
 * time.
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

//Add the conncetion.php file so that we are able to run our queries.
require("connection.php");

/**
 * This function will store the run details in the database.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 *
 */   
function recordRunButtonPressed($db, $date, $errorLogLocation) {
    //Check that the ajax post is not empty.
    if(!empty($_POST)) { 

        echo 'sucessMessageOne';
        //First query which will add data into research_data, everytime a research is executed.
        $query2 = "INSERT INTO research_data (research_data_id, latitude, time, time_in_mill)
        VALUES (:research_data_id, :latitude, :time, :time_in_mill)"; 

        //Parameter values.
        $query_params2 = array( 
    		':research_data_id' => $_POST['postSetEventId'],
    		':latitude' => 'Run', 
    		':time' => $_POST['postTime'],
    		':time_in_mill' => $_POST['postTimeInMill']
    	); 

        //Try/catch which will catch any errors and log the information to the servers log file.  
        try { 
            //Run query against db.
            $stmt2 = $db->prepare($query2); 
            $result2 = $stmt2->execute($query_params2); 
        } 
        catch(PDOException $ex2) { 
            //Kill the conncetion with an error message but also a server log. 
            error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex2->getMessage(), 3, $errorLogLocation);
            die("Failed to run query: " . $ex2->getMessage()); 
        }

        echo 'sucessMessageTwo';
        //Once the first query is done then execute the next one.
        if($result2 === TRUE) {

            //Second query will insert data into research_bridge.
            $query3 = "INSERT INTO research_bridge (research_id, research_data_id, user_id)
        	VALUES (:research_id, :research_data_id, :user_id)"; 

            //Parameter values.
            $query_params3 = array( 
                ':research_id' => $_POST['postSetResearchId'],
                ':research_data_id' => $_POST['postSetEventId'],
                ':user_id' => $_POST['postSetUserId'],
            ); 

            //Try/catch which will catch any errors and log the information to the servers log file.  
            try { 
                //Run query against db.
                $stmt3 = $db->prepare($query3); 
                $result3 = $stmt3->execute($query_params3); 
            } 
            catch(PDOException $ex3) { 
                //Kill the conncetion with an error message but also a server log. 
                error_log("Failed to run query: \n" . $ex3->getMessage(), 3, $errorLogLocation);
                die($date . " SQL-ERROR-102: Failed to run query: " . $ex3->getMessage()); 
            }
            echo 'sucessMessageThree';
        }
    }
}
recordRunButtonPressed($db, $date, $errorLogLocation);
?>
