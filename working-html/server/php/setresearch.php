<?php 
/**
 * Create a new research is handled.
 *
 * The appropriate tables will be populated with the 
 * users created research.
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

//Add the conncetion.php file so that we are able to run our queries.
require("connection.php"); 

/**
 * This function will handle the users create research event.
 *
 * Once the user has added all of their buttons to the concept keyboard
 * and they hit the create research button. This function will handle the 
 * event and populate the appropriate tables.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 */  
function researchCreatedEvent($db, $date, $errorLogLocation) {
    //Check that the ajax post is not empty.
    if(!empty($_POST)) { 

        //First query insert values into research_created table, these values are the new research that has been creted.
        $query = "INSERT INTO research_created (research_id, research_name, buttons_number, buttons_created, buttons_created_long_name)
        VALUES (:research_id, :research_name, :buttons_number, :buttons_created, :buttons_created_long_name)"; 

        //Parameter values.
        $query_params = array( 
            ':research_id' => $_POST['postSetResearchId'],
            ':research_name' => $_POST['postSetResearchName'],
            ':buttons_number' => $_POST['postSetButtonsNumber'],
            ':buttons_created' => $_POST['postSetButtonsCreated'],
            ':buttons_created_long_name' => $_POST['postSetButtonsCreatedLongName'] 
        ); 

        //Try/catch which will catch any errors and log the information to the servers log file.  
        try { 
            //Run query against db.
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) { 
            //Kill the conncetion with an error message but also a server log. 
            error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
            die("Failed to run query: " . $ex->getMessage()); 
        }

        //Once the results of the query have been inseted run the next query.
        echo 'sucessMessageOne';
        if($result === TRUE) {
            //Second query insert created research in the research_data, this will include the time when the research was created.
            $query2 = "INSERT INTO research_data (research_data_id, latitude, time, time_in_mill)
            VALUES (:research_data_id, :latitude, :time, :time_in_mill)"; 

            //Parameter values.
            $query_params2 = array( 
        		':research_data_id' => $_POST['postSetEventId'],
        		':latitude' => 'ResearchCreated', 
        		':time' => $_POST['postTime'],
        		':time_in_mill' => $_POST['postTimeInMill']
        	); 

            //Try/catch which will catch any errors and log the information to the servers log file.  
            try { 
                //Run query against db.
                $stmt2 = $db->prepare($query2); 
                $result2 = $stmt2->execute($query_params2); 
            } 
            catch(PDOException $ex2) { 
                //Kill the conncetion with an error message but also a server log. 
                error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex2->getMessage(), 3, $errorLogLocation);
                die("Failed to run query: " . $ex2->getMessage()); 
            }

            //Once the results of the query have been inseted run the next query.
            echo 'sucessMessageTwo';
            if($result2 === TRUE) {

                //Thierd query to insert into research_bridge.
                $query3 = "INSERT INTO research_bridge (research_id, research_data_id, user_id)
        		VALUES (:research_id, :research_data_id, :user_id)"; 

                //Parameter values.
                $query_params3 = array( 
                    ':research_id' => $_POST['postSetResearchId'],
                    ':research_data_id' => $_POST['postSetEventId'],
                    ':user_id' => $_POST['postSetUserId'],
                ); 

                try { 
                    // Execute the query against the database 
                    $stmt3 = $db->prepare($query3); 
                    $result3 = $stmt3->execute($query_params3); 
                } 
                catch(PDOException $ex3) { 
                    //Kill the conncetion with an error message but also a server log. 
                    error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex3->getMessage(), 3, $errorLogLocation);
                    die("Failed to run query: " . $ex3->getMessage()); 
                }
                echo 'sucessMessageThree';
    	    error_log($date . " USER-LOG-203: User " . $_POST['postSetUserId'] . " has created research " . $_POST['postSetResearchId'] . "\n", 3, $errorLogLocation); 
            }
        } else {
            echo "inuse";
            error_log($date . " USER-ERROR-305: Research id inuse.\n", 3, $errorLogLocation);
        }
    }
}
researchCreatedEvent($db, $date, $errorLogLocation);
?>
