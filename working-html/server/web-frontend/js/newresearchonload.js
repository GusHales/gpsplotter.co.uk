
//on refresh the data wount be lost
window.onload = function () {
    'use strict';
    document.getElementById('researchName').readOnly = false;
    document.getElementById("errorPane").style.visibility = "hidden";
    document.getElementById("errorMsg").style.visibility = "hidden";
    document.getElementById('researchId').value = localStorage.researchId;

    var researchName = JSON.parse(localStorage.getItem('researchName')) || "",
        oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
        oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
        oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
        formData = {},
        i,
        row;

    if (researchName !== "") {
        document.getElementById('researchName').value = researchName;
        document.getElementById('researchName').readOnly = true;
    }
    for (i = 0; i < oldButtons.length; i += 1) {
        formData.buttons = oldButtons[i];
        formData.buttonName = oldButtonsName[i];
        formData.buttonDesc = oldButtonsDesc[i];
        formData["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';

        row = $('<tr></tr>');
            
        $.each(formData, function (type, value) {
            $('<td class="input-' + type + '"></td>').html(value).appendTo(row);
        });
        $('.preview-table > tbody:last').append(row);
    }
};

setTimeout(function () {
    'use strict';
    document.getElementById("pageone").style.visibility = "visible";
}, 100);