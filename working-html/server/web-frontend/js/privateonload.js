function privatePost() {
    'use strict';
    var userId = JSON.parse(localStorage.getItem('userId')) || "",
        responseClean,
        responseArray,
        researchNames,
        researchIds,
        i,
        j;
    
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/getresearch.php',
        data: {postUserId: userId},
    
        success: function (response, status, xhr) {
            responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            responseArray = responseClean.split("-");
            researchNames = [];
            researchIds = [];
            responseArray.pop();
            
            if (responseClean === "0 resultsss") {
                console.log("no research found");
            }
                
            for (i = 0; i < responseArray.length; i += 1) {
                researchIds.push(responseArray[i]);
                researchNames.push(responseArray[i += 1]);
            }
            
            for (j = 0; j < researchNames.length; j += 1) {
                i = 0;

                $('#addr' + j).html("<td><h3 name='" + researchNames[j] + "' id='" + researchIds[j] + "'>" + researchNames[j] + "</h3> </td> <td id='optionsCell'><fieldset class='ui-grid-a'><div id='runViewButtons' class='ui-block-a'><button value='" + researchNames[j] + "' id='" + researchIds[j] + "'  onclick='handleView(this.id, this.value)' class='glyphicon glyphicon-chevron-right btn btn-primary btn-sm' data-icon='arrow-r' data-inline='true'>View</button></div><div id='runViewButtons' class='ui-block-b'><button id='" + researchIds[j] + "' onclick='handleRun(this.id)' class='glyphicon glyphicon-play btn btn-primary btn-sm' data-icon='arrow-r' data-inline='true'>Run</button></div></fieldset></td>");
                $('#tab_logic').append('<tr id="addr' + (j + 1) + '"></tr>');
            }
        
        },
    
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

window.onload = function () {
    'use strict';
    var userName = JSON.parse(localStorage.getItem('username')) || "",
        userId = JSON.parse(localStorage.getItem('userId')) || "";
    $("#userName").text(userName);
    privatePost();
};

setTimeout(function () {
    'use strict';
    document.getElementById("pageone").style.visibility = "visible";
}, 100);