// Generates research id
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}


//once new research is pressed the data will be cleared.
$('#newResearch').click(function () {
    'use strict';
    location.href = 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function () {
    'use strict';
    location.href = 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
});

$('#account').click(function () {
    'use strict';
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    location.href = "private.html";
});

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()