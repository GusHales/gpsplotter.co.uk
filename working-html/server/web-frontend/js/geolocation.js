var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP
}),
    markers = [],
    cityCircles = [],
    cityCircle,
    marker,
    r,
    i;

function addMarker(location, acc) {
    'use strict';
    var currentLocation = {
        position: location,
        map: map
    };
    var accuracyCircle = {
        strokeColor: '#619fdd',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#619fdd',
        fillOpacity: 0.35,
        map: map,
        center: location,
        radius: acc
    };
    console.log("here");
    marker = new google.maps.Marker(currentLocation);
    cityCircle = new google.maps.Circle(accuracyCircle);
    cityCircles.push(cityCircle);
    marker.setOpacity(1.0);
    markers.push(marker);
    setTimeout(function () {
        for(i = 0; i < markers.length; i += 1) {
            markers[i].setOpacity(0.25);
            cityCircles[i].setMap(null);
        }
    }, 5000);
    
    google.maps.event.addListener(marker,'click', (function(marker, cityCircle){ 
        return function() {
            marker.setOpacity(1.0);
            
            var accuracyCircle = {
                strokeColor: '#619fdd',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#619fdd',
                fillOpacity: 0.35,
                map: map,
                center: location,
                radius: acc
            };
            
            cityCircle.setOptions(accuracyCircle);
            setTimeout(function () {
                marker.setOpacity(0.25);
                cityCircle.setMap(null);
            }, 5000);
            
        };
    })(marker, cityCircle)); 
}


function handleNoGeolocation(errorFlag) {
    'use strict';
    var content,
        options,
        infowindow;
    if (errorFlag) {
        content = 'Error: The Geolocation service failed.';
    } else {
        content = 'Error: Your browser doesn\'t support geolocation.';
    }

    options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
    };

    infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
}

function initialize() {
    'use strict';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (location) {
            var loc = new google.maps.LatLng(location.coords.latitude,
                                       location.coords.longitude);

            map.setCenter(loc);
        }, function () {
            handleNoGeolocation(true);
        });
    } else {
    // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
}

//-- Map settings --//
google.maps.event.addDomListener(window, 'resize', initialize);
google.maps.event.addDomListener(window, 'load', initialize);

var b1 = $("#btn1")[0], b2 = $("#btn2")[0], b3 = $("#btn3")[0],
    b4 = $("#btn4")[0], b5 = $("#btn5")[0], b6 = $("#btn6")[0];

//concept keyboard action
$([b1, b2, b3, b4, b5, b6]).click(function () {
    'use strict';
    navigator.geolocation.getCurrentPosition(function (location) {
            var loc = new google.maps.LatLng(location.coords.latitude,
                                       location.coords.longitude);

        console.log(location.coords.speed);
        addMarker(loc, location.coords.accuracy);
        console.log(markers);
        
        var date = new Date(),
            time = date.getTime(),
            newDate = new Date(time),
            stringDate = String(newDate),
            eventId = generateEventId();
        
        document.getElementById('latText').value = location.coords.latitude;
        document.getElementById('longText').value = location.coords.longitude;
        document.getElementById('timeText').value = stringDate;
        document.getElementById('accText').value = location.coords.accuracy;
        document.getElementById('eventId').value = eventId;

        document.getElementById("result").innerHTML = "Data saved";
        post();
    });
});

// Generates research id
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

// Generate event id
function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function post() {
    'use strict';
    var latText = $('#latText').val(),
        longText = $('#longText').val(),
        timeText = $('#timeText').val(),
        accText = $('#accText').val(),
        researchId = $('#researchId').val(),
        userId = JSON.parse(localStorage.getItem('userId')) || "",
        btnText = $('#btnText').val(),
        resName = JSON.parse(localStorage.getItem('researchName')) || [],
        btnDesc = $('#btnDesc').val(),
        eventId = $('#eventId').val();
       
    $.ajax({
        type: 'POST',
        dataType: "text",
        url: 'http://gpsplotter.co.uk/php/server.php',
        data: {postUserId: userId, postResearchId: researchId, postLatText: latText, postLongText: longText, postTimeText: timeText, postAccText: accText, postBtnText: btnText,
                             postResName: resName[0], postBtnDesc: btnDesc, postEventId: eventId},
        success: function (response) {
            console.log(response);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

//done button action
$("#doneButton").click(function () {
    'use strict';
    r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
    if (r === true) {
        localStorage.setItem('buttonsArray', '[]');
        localStorage.setItem('buttonsNameArray', '[]');
        localStorage.setItem('buttonsDescArray', '[]');
        localStorage.setItem('researchName', '[]');
        localStorage.setItem('researchId', '[]');
        localStorage.setItem('researchedDataArray', '[]');
        setTimeout(function () {location.href = 'private.html'; }, 3000);
    }
});

//once new research is pressed the data will be cleared.
$('#newResearch').click(function () {
    'use strict';
    r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
    if (r === true) {
        location.href = 'newresearch.html';
        var researchId = generateResearchId();
        localStorage.setItem('buttonsArray', '[]');
        localStorage.setItem('buttonsNameArray', '[]');
        localStorage.setItem('buttonsDescArray', '[]');
        localStorage.setItem('researchName', '""');
        localStorage.setItem('researchId', researchId);
        localStorage.setItem('researchedDataArray', '[]');
    }
});

//once new research is pressed the data will be cleared.
$('#logout').click(function () {
    'use strict';
    r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
    if (r === true) {
        location.href = 'index.html';
        localStorage.setItem('buttonsArray', '[]');
        localStorage.setItem('buttonsNameArray', '[]');
        localStorage.setItem('buttonsDescArray', '[]');
        localStorage.setItem('researchName', '[]');
        localStorage.setItem('researchedDataArray', '[]');
        localStorage.setItem('researchId', '[]');
        localStorage.setItem('loginReply', '""');
        localStorage.setItem('username', '""');
        localStorage.setItem('userId', '""');
        localStorage.setItem('researchNamesArray', '[]');
        localStorage.setItem('researchIdsArray', '[]');
    }
});

$('#account').click(function () {
    'use strict';
    r = confirm("Press OK if you're done with the research and wich to exit research or Cancel if you're not done.");
    if (r === true) {
        location.href = "private.html";
    }
});
