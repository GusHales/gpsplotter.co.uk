window.onload = function () {
    'use strict';
    var researchId = JSON.parse(localStorage.getItem('viewResearchId')) || "",
        responseClean,
        responseArray,
        i,
        temparray,
        chunk,
        j;
    
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/viewresearch.php',
        data: {postViewResearch: researchId},
        success: function (response, status, xhr) {
            responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            responseArray = responseClean.split("|");
            responseArray.pop();
            localStorage.setItem('exportDataArray', JSON.stringify(responseArray));
            chunk = 6;
            j = 0;
            for (i = 0; i < responseArray.length; i += chunk) {
                temparray = responseArray.slice(i, i + chunk);
            
                $('#addr' + j).html(" <td><h4 name='" + temparray[0] + "' id='researchName'>" + temparray[0] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[1] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[2] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[3] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[4] + "</h3></td><td><h4 name='researchName' id='researchName'>" + temparray[5] + "</h4></td>");
                $('#tab_logic').append('<tr id="addr' + (j + 1) + '"></tr>');
                j += 1;
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
};

setTimeout(function () {
    'use strict';
    document.getElementById("pageone").style.visibility = "visible";
}, 100);