// Generate event id
function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function postNewResearch() {
    'use strict';
    var researchId = localStorage.researchId,
        researchName = JSON.parse(localStorage.getItem('researchName')) || [],
        buttonsNumber = localStorage.getItem('buttonsArray') || [],
        buttonsCreated = localStorage.getItem('buttonsNameArray') || [],
        buttonsCreatedLongName = localStorage.getItem('buttonsDescArray') || [],
        eventId = generateEventId(),
        userId = JSON.parse(localStorage.getItem('userId')) || "";
        
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/setresearch.php',
        data: {postSetResearchId: researchId, postSetResearchName: researchName.toString(), postSetButtonsNumber: buttonsNumber, postSetButtonsCreated: buttonsCreated, postSetButtonsCreatedLongName: buttonsCreatedLongName, postSetEventId: eventId, postSetUserId: userId},
        success: function (response, status, xhr) {
            var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            console.log(response);
        },
        error: function (xhr, status, error) {
            console.log("here3");
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

$("#createResearch").click(function () {
    'use strict';
    var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
        oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
        oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
        finalButtonArray = [],
        finalButtonDescArray = [],
        i;
    
    postNewResearch();
    
    setTimeout(function () {
        if (oldButtons.length >= 1) {
            location.href = 'research.html';
            for (i = 0; i < oldButtons.length; i += 1) {
                finalButtonArray.push(oldButtons[i]);
                finalButtonArray.push(oldButtonsName[i]);
                finalButtonDescArray.push(oldButtons[i]);
                finalButtonDescArray.push(oldButtonsDesc[i]);
            }
            localStorage.setItem('finalButtonArray', JSON.stringify(finalButtonArray));
            localStorage.setItem('finalButtonDescArray', JSON.stringify(finalButtonDescArray));
        } else {
            document.getElementById("errorPane").style.visibility = "visible";
            document.getElementById("errorMsg").style.visibility = "visible";
            document.querySelector("#errorMsg").innerHTML = "Error: Please add atleast two buttons.";
        }
    }, 3000);
});

// Generates research id
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

//once new research is pressed the data will be cleared.
$('#newResearch').click(function () {
    'use strict';
    location.href = 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function () {
    'use strict';
    location.href = 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
});

$('#account').click(function () {
    'use strict';
    location.href = "private.html";
});



$(document).on('click', '.input-remove-row', function () {
    'use strict';
    var tr = $(this).closest('tr'),
        oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
        oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
        oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
        i,
        remover;
    
    tr.fadeOut(200, function () {
        tr.remove();
        remover = tr[0].innerText;
        for (i = 0; i < oldButtons.length; i += 1) {
            if (remover.indexOf(tr[0].innerText.substring(0, 4)) > -1) {
                if (oldButtons[i] === tr[0].innerText.substring(0, 4)) {
                    oldButtons.splice(i, 1);
                    oldButtonsName.splice(i, 1);
                    oldButtonsDesc.splice(i, 1);
                }
            }
            localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
            localStorage.setItem('buttonsNameArray', JSON.stringify(oldButtonsName));
            localStorage.setItem('buttonsDescArray', JSON.stringify(oldButtonsDesc));
        }
	});
});

$(function () {
    'use strict';
    $("#researchName").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("buttons").focus();
        }
    });
     
    $("#buttonName").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("buttonDesc").focus();
        }
    });
    
    $("#buttonDesc").keydown(function (e) {
        if (e.keyCode === 13) {
            document.getElementById("addButton").focus();
        }
    });
});

$("#searchclear").click(function () {
    'use strict';
    document.getElementById('researchName').readOnly = false;
    $("#researchName").val('');
});

$(function () {
    'use strict';
    $('.preview-add-button').click(function () {
        var formData = {},
            researchName = JSON.parse(localStorage.getItem('researchName')) || "",
            oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
            oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
            oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
            row;
    
        if ($.inArray($('.research-form #buttons option:selected').text(), oldButtons) !== -1) {
            document.getElementById("errorPane").style.visibility = "visible";
            document.getElementById("errorMsg").style.visibility = "visible";
            document.querySelector("#errorMsg").innerHTML = "Error: " + $('.research-form #buttons option:selected').text()  + " button already added.";
        } else {
            if ($('.research-form #buttons option:selected').text() !== "Select") {
                document.getElementById("errorPane").style.visibility = "hidden";
                document.getElementById("errorMsg").style.visibility = "hidden";
                if ($('.research-form input[name="buttonName"]').val() !== "") {
                    formData.buttons = $('.research-form #buttons option:selected').text();
                    formData.buttonName = $('.research-form input[name="buttonName"]').val();
                    formData.buttonDesc = $('.research-form input[name="buttonDesc"]').val();
                    formData["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';
                    oldButtonsName.push($('.research-form input[name="buttonName"]').val());
                    oldButtons.push($('.research-form #buttons option:selected').text());
                    oldButtonsDesc.push($('.research-form input[name="buttonDesc"]').val());
                    researchName = [];
                    researchName.push($('.research-form input[name="researchName"]').val());
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.querySelector("#errorMsg").innerHTML = "Error: * Please enter required fields.";
                }
                if (researchName !== "") {
                    document.getElementById("researchName").readOnly = true;
                    document.getElementById("buttonName").value = "";
                    document.getElementById("buttonDesc").value = "";
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.querySelector("#errorMsg").innerHTML = "Error: * Please enter required fields.";
                }
            } else {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.querySelector("#errorMsg").innerHTML = "Error: * Please enter required fields.";
            }
        }
        
        row = $('<tr></tr>');
        $.each(formData, function (type, value) {
            $('<td class="input-' + type + '"></td>').html(value).appendTo(row);
        });
        $('.preview-table > tbody:last').append(row);
        
        localStorage.setItem('researchName', JSON.stringify(researchName));
        localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
        localStorage.setItem('buttonsNameArray', JSON.stringify(oldButtonsName));
        localStorage.setItem('buttonsDescArray', JSON.stringify(oldButtonsDesc));
    });
});
