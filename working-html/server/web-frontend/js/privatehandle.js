// Generates research id
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

//once new research is pressed the data will be cleared.
$('#newResearch').click(function () {
    'use strict';
    location.href = 'newresearch.html';
    var researchId = generateResearchId();
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchId', researchId);
    localStorage.setItem('researchedDataArray', '[]');
});

//once new research is pressed the data will be cleared.
$('#logout').click(function () {
    'use strict';
    location.href = 'index.html';
    localStorage.setItem('buttonsArray', '[]');
    localStorage.setItem('buttonsNameArray', '[]');
    localStorage.setItem('buttonsDescArray', '[]');
    localStorage.setItem('researchName', '""');
    localStorage.setItem('researchedDataArray', '[]');
    localStorage.setItem('researchId', '[]');
    localStorage.setItem('loginReply', '""');
    localStorage.setItem('username', '""');
    localStorage.setItem('userId', '""');
    localStorage.setItem('researchNamesArray', '[]');
    localStorage.setItem('researchIdsArray', '[]');
});

$('#account').click(function () {
    'use strict';
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    location.href = "private.html";
});



function handleRun(button_id) {
    'use strict';
    var researchId = button_id.replace(/(?:\r\n|\r|\n)/g, ''),
        responseClean,
        responseArray,
        researchName,
        researchBtns,
        researchBtnsArray,
        researchBtnsNames,
        researchBtnsNamesArray,
        researchBtnsLongNames,
        researchBtnsLongNamesArray,
        oldButtons,
        oldButtonsName,
        oldButtonsDesc,
        finalButtonArray,
        finalButtonDescArray,
        i;

    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'http://gpsplotter.co.uk/php/requestresearch.php',
        data: {postRequestResearchId: researchId},
    
        success: function (response, status, xhr) {
            
            responseClean = response.replace(/(?:\r\n|\"|\]|\[|\r|\n)/g, '');
            responseArray = responseClean.split("-");
            researchName = responseArray[0];
            researchBtns = responseArray[1];
            researchBtnsArray = researchBtns.split(",");
            researchBtnsNames = responseArray[2];
            researchBtnsNamesArray = researchBtnsNames.split(",");
            researchBtnsLongNames = responseArray[3];
            researchBtnsLongNamesArray = researchBtnsLongNames.split(",");
            oldButtons = researchBtnsArray;
            oldButtonsName = researchBtnsNamesArray;
            oldButtonsDesc = researchBtnsLongNamesArray;
            finalButtonArray = [];
            finalButtonDescArray = [];

            localStorage.setItem('researchName', JSON.stringify(researchName));
            localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
            localStorage.setItem('researchId', researchId);
            
            setTimeout(function () {
                if (oldButtons.length >= 1) {
                    location.href = 'research.html';
                    for (i = 0; i < oldButtons.length; i += 1) {
                        finalButtonArray.push(oldButtons[i]);
                        finalButtonArray.push(oldButtonsName[i]);
                        finalButtonDescArray.push(oldButtons[i]);
                        finalButtonDescArray.push(oldButtonsDesc[i]);
                    }
                
                    localStorage.setItem('finalButtonArray', JSON.stringify(finalButtonArray));
                    localStorage.setItem('finalButtonDescArray', JSON.stringify(finalButtonDescArray));
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.querySelector("#errorMsg").innerHTML = "Error: Please add atleast two buttons.";
                }
            }, 3000);

        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

function handleView(button_id, researchName) {
    'use strict';
    var researchId = button_id.replace(/(?:\r\n|\r|\n)/g, '');
    localStorage.setItem('viewResearchName', JSON.stringify(researchName));
    localStorage.setItem('viewResearchId', JSON.stringify(researchId));
    location.href = "researchview.html";
}

$('#refresh').click(function () {
    'use strict';
    var userId = JSON.parse(localStorage.getItem('userId')) || "";
    location.href = "private.html";
});