<?php
/**
 * Handle connections for all files.
 *
 * All of the files are split into their appropriate functions which will
 * called when an ajax call happens in the client side, this connection file
 * will be required by all the other files in order to be able to connect to
 * the database.
 *
 */ 

//Connection to the database via the setting below.
$username = "qdb11141";
$password = "oonsiedr";
$host = "devweb2014.cis.strath.ac.uk";
$dbname = "qdb11141";
$errorLogLocation = "/home/student/q/qdb11141/DEVWEB/2014/php/php_errors.log";
date_default_timezone_set('Europe/London');
$date = date('m/d/Y h:i:s a', time());
//Options array for the database connection which will tell MySQL that we want to
//use UTF-8

$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

/**
 * Handle the connection to the database.
 *
 * @param string $username Database username
 * @param string $password Database password
 * @param string $host Database host
 * @param string $dbname Database name
 * @param string $errorLogLocation File location for the log
 * @param string $date Current date/time
 * @param array $options Database options
 * @return mixed $db Returns a php database object
 */
function handleConnection($username, $password, $host, $dbname, $errorLogLocation, $date, $options) {
    //Error handling with the try catch statement, if any error eccoure the code will move
    //to catch that error and log it on the server log.
    try {
        //Create a conncetion to the database with the variables from above, PDO provides 
        //flexiblity among variouse databases so that the system is not just stuck to MySQL.
        $db = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options); 
    }
    catch(PDOException $ex) {
        //If any error occur log the error and kill the conncetion.
        error_log($date . " SQL-ERROR-101: Failed to connect to the database: \n" . $ex->getMessage(), 3, $errorLogLocation);
        die("Failed to connect to the database: " . $ex->getMessage());
    }

    //Sets PDO to thow ecxeption when an error is encountered.
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //Sets PDO to return the database rows so that the values mat be extracted depending 
    //on the situation.
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $db;
}

$db = handleConnection($username, $password, $host, $dbname, $errorLogLocation, $date, $options);

/**
 * Remove magic quotes just incase they are used even though they have been removed from the current version of PHP.
 *
 */
function fixMagicQuotes() {
    if(function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()) {
        doFix($_POST);
        doFix($_GET);
        doFix($_COOKIE);
    }
}

fixMagicQuotes();

/**
 * After check apply fix.
 * 
 * @param array @array Array for post/get/cookie
 */
function doFix(&$array) {
    foreach($array as &$value) {
        if(is_array($value)) {
            doFix($value);
        } else {
            $value = stripslashes($value);
        }
    }
}
?>

