<?php 
/**
 * Handle user register action.
 *
 * Once the user creates an account this will handle that event,
 * the password will be hashed and ther is sufficient error check.
 *
 */  

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');

//Add the conncetion.php file so that we are able to run our queries.
require("connection.php"); 

/**
 * This function will register a user.
 *
 * When a user creates an account the username must be unique,
 * and the email address must be valid. This function will check
 * for any mistakes and reply back to the user. The password will
 * also be hashed for security.
 *
 * @param mixed $db PHP Database Object
 * @param string $date Date for the log file
 * @param string $errorLogLocation Location for the log file
 *
 */   
function registerUser($db, $date, $errorLogLocation) {
    //Check that the ajax post is not empty.
    if(!empty($_POST)) { 

        //Check that the username/password enter are not empty and that the email address is a valid one.
        if(empty($_POST['postUsername'])) { 
            //Message sent back to the client side which will print approriate message.
            echo "enterUsername";
            error_log($date . " USER-ERROR-303: Username is empty.\n", 3, $errorLogLocation);
        } else if(empty($_POST['postPassword'])) { 
            //Message sent back to the client side which will print approriate message.
            echo "enterPassword";
            error_log($date . " USER-ERROR-303: Password is empty.\n", 3, $errorLogLocation);
        } else if(!filter_var($_POST['postEmail'], FILTER_VALIDATE_EMAIL)) { 
            //Message sent back to the client side which will print approriate message.
            echo "invalidEmail";
            error_log($date . " USER-ERROR-303: Invalid email address.\n", 3, $errorLogLocation);
        } else {
         
        //First query to check weather the username is in use.
        $query = "SELECT 1 FROM users WHERE username = :username"; 
         
        //Parameter values.
        $query_params = array( 
            ':username' => $_POST['postUsername'] 
        ); 
        
        //Try/catch which will catch any errors and log the information to the servers log file.  
        try { 
            //Run query against db.
            $stmt = $db->prepare($query); 
            $result = $stmt->execute($query_params); 
        } 
        catch(PDOException $ex) { 
            //Kill the conncetion with an error message but also a server log. 
            error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
            die("Failed to run query: " . $ex->getMessage()); 
        } 
         
        //Retrive the requested information. If results exists then continue.
        $row = $stmt->fetch(); 
        if($row) { 
            //Message sent back to the client side which will print approriate message.
            echo "usernameInUse";
            error_log($date . " USER-ERROR-304: Username in use.\n", 3, $errorLogLocation);
        } else {

            //Second query to check weather the email is in use.
            $query = "SELECT 1 FROM users WHERE email = :email"; 

            //Parameter values.
            $query_params = array( 
                ':email' => $_POST['postEmail'] 
            ); 

            //Try/catch which will catch any errors and log the information to the servers log file.  
            try { 
                //Run query against db.
                $stmt = $db->prepare($query); 
                $result = $stmt->execute($query_params); 
            } 
            catch(PDOException $ex) { 
                //Kill the conncetion with an error message but also a server log. 
                error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
                die("Failed to run query: " . $ex->getMessage()); 
            } 

            //Retrive the requested information. If results exists then continue.
            $row = $stmt->fetch(); 
            if($row) { 
                //Message sent back to the client side which will print approriate message.
                echo "emailInUse";
            } else {

                // Thierd query which will insert the new users details into the table.
                $query = "INSERT INTO users (user_id, username, password, salt, email) 
                VALUES ( :userid, :username, :password, :salt, :email)"; 

                //To protect against brute force attacks a salt is generated from a 8 byte hex.
                $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 

                //Then the salt is used to hash the password which produces a 64 byte hex string.
                $password = hash('sha256', $_POST['postPassword'] . $salt); 

                //Now to provide furted security the hash gets hashed 65536 more times.
                for($round = 0; $round < 65536; $round++) { 
                    $password = hash('sha256', $password . $salt); 
                } 

                $userid = $_POST['postUserId'];

                //Parameter values.
                $query_params = array( 
                    ':userid' => $userid, 
                    ':username' => $_POST['postUsername'], 
                    ':password' => $password, 
                    ':salt' => $salt, 
                    ':email' => $_POST['postEmail'] 
                ); 

                //Try/catch which will catch any errors and log the information to the servers log file.  
                try { 
                    //Run query against db.
                    $stmt = $db->prepare($query); 
                    $result = $stmt->execute($query_params); 
                } 
                catch(PDOException $ex) { 
                    //Kill the conncetion with an error message but also a server log. 
                    error_log($date . " SQL-ERROR-102: Failed to run query: \n" . $ex->getMessage(), 3, $errorLogLocation);
                    die("Failed to run query: " . $ex->getMessage()); 
                } 

                echo "login.html";
                }
            }
        }
    } 
}
registerUser($db, $date, $errorLogLocation);
?> 

