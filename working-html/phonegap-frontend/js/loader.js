/**
  * Give time to the page to load all elements, once loaded make them visible.
  */
function letMeLoad() {
	setTimeout(function () {
	    'use strict';
	    document.getElementById("pageone").style.visibility = "visible";
	}, 100);
}

/**
  * Show AJAX spinner.
  */
function showSpinner(){
    $.mobile.loading("show");
}

/**
  * Hide AJAX spinner.
  */    
function hideSpinner(){
    $.mobile.loading("hide");
}

var content = $.mobile.getScreenHeight() - $(".ui-header").outerHeight() - $(".ui-footer").outerHeight() - $(".ui-content").outerHeight() + $(".ui-content").height();
$(".ui-content").height(content);

letMeLoad();