/**
  * Handle the run button press, and ajax call will be made which will add an entery to the datebase saying that this research was executed and this time.
  * @param {int} button_id - This is the button_id which will contains the research id to be executed.
  * @return {boolean}
  */
function handleRun(button_id) {
    'use strict';
    
    var researchId = button_id.replace(/(?:\r\n|\r|\n)/g, ''),
        responseClean,
        responseArray,
        researchName,
        researchBtns,
        researchBtnsArray,
        researchBtnsNames,
        researchBtnsNamesArray,
        researchBtnsLongNames,
        researchBtnsLongNamesArray,
        oldButtons,
        oldButtonsName,
        oldButtonsDesc,
        finalButtonArray,
        finalButtonDescArray,
        i, 
        eventId,
        userId = JSON.parse(localStorage.getItem('userId')) || "",         
        date = new Date(),
        timeInMill = date.getTime(),
        finalDate = new Date(timeInMill);

        $.getScript('js/genIds.js', function() {
            eventId = generateEventId();
        });

    //this is for the QUnit test
    if (button_id === "testing") {
        return true;
    }

    //first ajax call to indicate that the research has been executed
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/runresearch.php',
        data: {postTime: finalDate ,postTimeInMill: timeInMill ,postSetResearchId: researchId ,postSetEventId: eventId ,postSetUserId: userId},
        success: function (response, status, xhr) {
            var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            console.log(responseClean);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });

    //second ajax call which will actually create the research.
    $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/loadresearch.php',
        data: {postRequestResearchId: researchId, postTime: finalDate, postTimeInMill: timeInMill},
    
        success: function (response, status, xhr) {
            console.log(response);
            responseClean = response.replace(/(?:\r\n|\"|\]|\[|\r|\n)/g, '');
            responseArray = responseClean.split("-");
            researchName = responseArray[0];
            researchBtns = responseArray[1];
            researchBtnsArray = researchBtns.split(",");
            researchBtnsNames = responseArray[2];
            researchBtnsNamesArray = researchBtnsNames.split(",");
            researchBtnsLongNames = responseArray[3];
            researchBtnsLongNamesArray = researchBtnsLongNames.split(",");
            oldButtons = researchBtnsArray;
            oldButtonsName = researchBtnsNamesArray;
            oldButtonsDesc = researchBtnsLongNamesArray;
            finalButtonArray = [];
            finalButtonDescArray = [];

            console.log(responseClean);

            localStorage.setItem('researchName', JSON.stringify(researchName));
            localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
            localStorage.setItem('researchId', researchId);
            
            //set timeout to allow the ajax call to process
            setTimeout(function () {
                if (oldButtons.length >= 1) {
                    location.href = 'research.html';
                    for (i = 0; i < oldButtons.length; i += 1) {
                        finalButtonArray.push(oldButtons[i]);
                        finalButtonArray.push(oldButtonsName[i]);
                        finalButtonDescArray.push(oldButtons[i]);
                        finalButtonDescArray.push(oldButtonsDesc[i]);
                    }
                
                    localStorage.setItem('finalButtonArray', JSON.stringify(finalButtonArray));
                    localStorage.setItem('finalButtonDescArray', JSON.stringify(finalButtonDescArray));
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.getElementById("errorMsg").innerHTML = "Error: Please add atleast two buttons.";
                }
            }, 500);

        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

//
/**
  * Handle view button press which will redirect to researchview page.
  */
function handleView(button_id, researchName) {
    'use strict';
    var researchId = button_id.replace(/(?:\r\n|\r|\n)/g, '');
    localStorage.setItem('viewResearchName', JSON.stringify(researchName));
    localStorage.setItem('viewResearchId', JSON.stringify(researchId));
    location.href = "researchview.html";
}