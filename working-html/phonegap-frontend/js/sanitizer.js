/**
  * Function which will sanitize any input from the user.
  * @param {string} s - Input string which will sanitize.
  */
function sanitizer(s) {
    return s.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}