/**
  * User approves of the ethics.
  */
function handleContinueEvent() {
    $("#continue").click(function () {
        'use strict';
        var terms = document.getElementById("checkbox-1").checked,
            researchId = generateResearchId();
        if (terms === true) {
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '[]');
            localStorage.setItem('researchId', researchId);
            localStorage.setItem('researchedDataArray', '[]');
            
            location.href = 'moreinfo.html';
        } else {
            location.href = 'index.html';
        }
    });
}

handleContinueEvent();