/**
  * Handle export button.
  */
function handleExportEvent() {
  $('#export').click(function () {
      'use strict';
      window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, gotFS, fail);
      localStorage.setItem('fileLocation', '""');
  });
}

/**
  * Function which will take a writer and write to file.
  * @param {IOWriter} writer - IO Writer.
  */
function gotFileWriter(writer) {
    'use strict';
    var fileName = JSON.parse(localStorage.getItem('vi∂ewResearchName')) + "-" + JSON.parse(localStorage.getItem('viewResearchId')),
        exportDataArray = JSON.parse(localStorage.getItem('exportDataArray')) || [],
        researchName = "Research name = " + JSON.parse(localStorage.getItem('viewResearchName')) || "",
        headings = ["Latitude","Longitude","Accuracy","Heading","Speed","Time","Time_in_Mill","Button_Name","Button_Long_Name"],
        i,
        temparray,
        chunk = 9,
        j = 0,
        writerString = "\n" + "\n" + headings,
        fileLocation = JSON.parse(localStorage.getItem('fileLocation')) || "";
    
    alert("The file " + fileName + ".txt has been written to, " + fileLocation);

    for (i = 0; i < exportDataArray.length; i += chunk) {
        temparray = exportDataArray.slice(i, i + chunk);
        if (temparray[0] !== "" || temparray[1] !== "" || temparray[2] !== "" || temparray[3] !== ""
                || temparray[4] !== "" || temparray[5] !== "") {
            writerString = writerString + "\n";
            writerString = writerString + temparray;
        }
    }
    console.log(writerString);
    writer.write(writerString);
}

/**
  * Function which will take a writer and write to file.
  * @param {string} error - Writter error message.
  */
function fail(error) {
    'use strict';
    alert("Error file did not write!");
}

/**
  * Check device has file access plugin.
  * @param {string} fileEntry
  */
function gotFileEntry(fileEntry) {
    'use strict';
    fileEntry.createWriter(gotFileWriter, fail);
}

/**
  * Get system path and access.
  * @param {string} fileSystem
  */
function gotFS(fileSystem) {
    'use strict';
    var fileName = JSON.parse(localStorage.getItem('viewResearchName')) + "-" + JSON.parse(localStorage.getItem('viewResearchId')) + ".csv";
    fileSystem.root.getFile(fileName, {create: true, exclusive: false},  gotFileEntry, fail);
    localStorage.setItem('fileLocation', JSON.stringify(fileSystem.root.nativeURL));
}

handleExportEvent();