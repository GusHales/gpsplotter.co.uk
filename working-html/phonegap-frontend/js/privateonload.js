/**
  * Handle backbutton press on android.
  */
function onBackKeyDown() {
    'use strict';
    location.href = "private.html";
}

/**
  * Private post will handle the table that gets loaded at the start, this will retriave all the research that the user has created.
  * @return {ajaxObject}
  */
function privatePost() {    
    'use strict';
    var userId = JSON.parse(localStorage.getItem('userId')) || "",
        responseClean,
        responseArray,
        researchNames,
        researchIds,
        i,
        j;
    
    return $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/getresearch.php',
        data: {postUserId: userId},
    
        success: function (response, status, xhr) {
            responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            responseArray = responseClean.split("|");
            researchNames = [];
            researchIds = [];
            responseArray.pop();
            //populate table
            for (i = 0; i < responseArray.length; i += 1) {
                researchIds.push(responseArray[i]);
                researchNames.push(responseArray[i += 1]);
            }
            for (j = 0; j < researchNames.length; j += 1) {
                // i = 0;

                $('#addr' + j).html("<td><h3 name='" + researchNames[j] + "' id='" + researchIds[j] + "'>" + researchNames[j] + "</h3> </td> <td id='optionsCell'><fieldset class='ui-grid-a'><div id='runViewButtons' class='ui-block-a'><button value='" + researchNames[j] + "' id='" + researchIds[j] + "'  onclick='handleView(this.id, this.value)' class='glyphicon glyphicon-chevron-right btn btn-primary btn-sm' data-icon='arrow-r' data-inline='true'>View</button></div><div id='runViewButtons' class='ui-block-b'><button id='" + researchIds[j] + "' onclick='handleRun(this.id)' class='glyphicon glyphicon-play btn btn-primary btn-sm' data-icon='arrow-r' data-inline='true'>Run</button></div></fieldset></td>");
                $('#tab_logic').append('<tr id="addr' + (j + 1) + '"></tr>');
            }
        
        },
        
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

/**
  * This function will call the table sorting framework once the ajax call has been made.
  */
function waitForAJAX() {
    $.when(privatePost()).done(function(a1) {
        hideSpinner();
    });

}

/**
  * Handle device ready
  */
function onDeciveReady() {
    'use strict';
    var userName = JSON.parse(localStorage.getItem('username')) || "",
        userId = JSON.parse(localStorage.getItem('userId')) || "";
    $("#userName").text(userName);
    // privatePost();
    document.addEventListener("backbutton", onBackKeyDown, false);
    waitForAJAX();
}

/**
  * Body calls onLoad when the page is ready to load.
  */
function onLoad() {
    'use strict';
    showSpinner();
    document.addEventListener("deviceready", onDeciveReady);
}