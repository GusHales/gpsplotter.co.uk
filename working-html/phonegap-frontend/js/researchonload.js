/**
  * Handle back button.
  */
function onBackKeyDown() {
    'use strict';
    location.href = "research.html";
}

/**
  * Handle a refresh on the page and once the device is ready set the buttons.
  */
function onDeciveReady() {
    'use strict';
    document.addEventListener("backbutton", onBackKeyDown, false);
    var resName = JSON.parse(localStorage.getItem('researchName')) || [],
        oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
        btnName = JSON.parse(localStorage.getItem('finalButtonArray')) || [],
        btnDesc = JSON.parse(localStorage.getItem('finalButtonDescArray')) || [],
        btn = "",
        splitValue = "",
        i;

    document.getElementById('researchId').value = localStorage.researchId;
    document.getElementById('researchNameHeader').value = resName;
    document.getElementById('researchNameHeader').innerHTML = "Research: " + resName;
    
    for (i = 0; i < oldButtons.length; i += 1) {
        if (oldButtons.indexOf("btn1") !== -1) {
            $("#btn1").show();
            document.getElementById('btn1').innerHTML = btnName[btnName.indexOf("btn1") + 1];
        }
        if (oldButtons.indexOf("btn2") !== -1) {
            $("#btn2").show();
            document.getElementById('btn2').innerHTML = btnName[btnName.indexOf("btn2") + 1];
        }
        if (oldButtons.indexOf("btn3") !== -1) {
            $("#btn3").show();
            document.getElementById('btn3').innerHTML = btnName[btnName.indexOf("btn3") + 1];
        }
        if (oldButtons.indexOf("btn4") !== -1) {
            $("#btn4").show();
            document.getElementById('btn4').innerHTML = btnName[btnName.indexOf("btn4") + 1];
        }
        if (oldButtons.indexOf("btn5") !== -1) {
            $("#btn5").show();
            document.getElementById('btn5').innerHTML = btnName[btnName.indexOf("btn5") + 1];
        }
        if (oldButtons.indexOf("btn6") !== -1) {
            $("#btn6").show();
            document.getElementById('btn6').innerHTML = btnName[btnName.indexOf("btn6") + 1];
        }
    }
    //get button name and desc
    $('#btn1, #btn2, #btn3, #btn4, #btn5, #btn6').focusin(function () {
        if (this.id === 'btn1') {
            btn =  document.getElementById('btn1').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn1") + 1];
        } else if (this.id === 'btn2') {
            btn = document.getElementById('btn2').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn2") + 1];
        } else if (this.id === 'btn3') {
            btn = document.getElementById('btn3').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn3") + 1];
        } else if (this.id === 'btn4') {
            btn = document.getElementById('btn4').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn4") + 1];
        } else if (this.id === 'btn5') {
            btn = document.getElementById('btn5').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn5") + 1];
        } else if (this.id === 'btn6') {
            btn = document.getElementById('btn6').innerHTML;
            document.getElementById('btnText').value = btn;
            document.getElementById('btnDesc').value = btnDesc[btnDesc.indexOf("btn6") + 1];
        }
    });
}

/**
  * Handle loading.
  */
function onLoad() {
    'use strict';
    document.addEventListener("deviceready", onDeciveReady);
    localStorage.setItem('researchedDataArray', '[]');
}