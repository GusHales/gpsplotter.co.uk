/**
  * Generate research data id.
  * @return {int}
  */
function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

/**
  * Generates research id.
  * @return {int}
  */
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}