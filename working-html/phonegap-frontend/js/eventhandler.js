/**
  * Once new research is pressed the data will be cleared.
  */
function handleNewResearchEvent() {
  $('#newResearch').click(function () {
      'use strict';
      location.href = 'newresearch.html';
      var researchId = generateResearchId();
      localStorage.setItem('buttonsArray', '[]');
      localStorage.setItem('buttonsNameArray', '[]');
      localStorage.setItem('buttonsDescArray', '[]');
      localStorage.setItem('researchName', '""');
      localStorage.setItem('researchId', researchId);
      localStorage.setItem('researchedDataArray', '[]');
  });
}

/**
  * Once new research is pressed the data will be cleared.
  */
function handleLogOutEvent() {
  $('#logout').click(function () {
      'use strict';
      location.href = 'index.html';
      localStorage.setItem('buttonsArray', '[]');
      localStorage.setItem('buttonsNameArray', '[]');
      localStorage.setItem('buttonsDescArray', '[]');
      localStorage.setItem('researchName', '""');
      localStorage.setItem('researchedDataArray', '[]');
      localStorage.setItem('researchId', '[]');
      localStorage.setItem('loginReply', '""');
      localStorage.setItem('username', '""');
      localStorage.setItem('userId', '""');
      localStorage.setItem('researchNamesArray', '[]');
      localStorage.setItem('researchIdsArray', '[]');
  });
}

/**
  * Handle account click.
  */
function handleAccountEvent() {
  $('#account').click(function () {
    'use strict';
    location.href = "private.html";
  });
}

/**
  * Handle home click.
  */
function handleHomePageEvent() {
  $('#homePage').click(function () {
    'use strict';
    location.href = 'index.html';
  });
}

/**
  * Handle login click.
  */
function handleLogInEvent() {
  $("#login").click(function () {
    'use strict';
    location.href = 'login.html';
  });
}

handleNewResearchEvent();
handleLogOutEvent();
handleAccountEvent();
handleHomePageEvent();
handleLogInEvent();