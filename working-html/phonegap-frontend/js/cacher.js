/**
  * This function will be called when the network is down and data needs to be cached.
  */
function dataCacher() {
    'use strict';
    var latText = $('#latText').val() || "",
        longText = $('#longText').val() || "",
        timeText = $('#timeText').val() || "",
        accText = $('#accText').val() || "",
        researchId = $('#researchId').val() || "",
        userId = $('#userId').val() || "",
        btnText = $('#btnText').val() || "",
        resName = JSON.parse(localStorage.getItem('researchName')) || "",
        btnDesc = $('#btnDesc').val() || "",
        eventId = $('#eventId').val() || "",
        researchedDataArray = JSON.parse(localStorage.getItem('researchedDataArray')) || [];
        
    researchedDataArray.push(latText.toString());
    researchedDataArray.push(longText.toString());
    researchedDataArray.push(timeText.toString());
    researchedDataArray.push(accText.toString());
    researchedDataArray.push(researchId.toString());
    researchedDataArray.push(userId.toString());
    researchedDataArray.push(btnText.toString());
    researchedDataArray.push(resName.toString());
    researchedDataArray.push(btnDesc.toString());
    researchedDataArray.push(eventId.toString());
    
    localStorage.setItem('researchedDataArray', JSON.stringify(researchedDataArray));
}

/**
  * This will then be called when network is avilabe and the data that is cached is ready to be flushed to the db.
  * @return {boolean}
  */
function cacherPost() {
    'use strict';
    var researchedDataArray = JSON.parse(localStorage.getItem('researchedDataArray')),
        latText = "",
        longText = "",
        timeText = "",
        accText = "",
        researchId = "",
        userId = "",
        btnText = "",
        resName = "",
        btnDesc = "",
        eventId = "",
        size,
        i,
        smallarray;
    
    if (researchedDataArray !== -1) {
        size = 10;
        for (i = 0; i < researchedDataArray.length; i += size) {
            smallarray = researchedDataArray.slice(i, i + size);
            latText = smallarray[0];
            longText = smallarray[1];
            timeText = smallarray[2];
            accText = smallarray[3];
            researchId = smallarray[4];
            userId = smallarray[5];
            btnText = smallarray[6];
            resName = smallarray[7];
            btnDesc = smallarray[8];
            eventId = smallarray[9];
        
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/populateresearch.php',
                data: {postUserId: userId, postResearchId: researchId, postLatText: latText, postLongText: longText, postTimeText: timeText, postAccText: accText, postBtnText: btnText,
                                     postResName: resName[0], postBtnDesc: btnDesc, postEventId: eventId},
                success: function (response) {
                    //reply to the result div tag
                },
                error: function (xhr, status, error) {
                    console.log(xhr);
                    console.log(status);
                    console.log(error);
                }
            });
        }
        return false;
    }
}