/**
  * Handle more infor click.
  */
function handleMoreInfoEvent() {
	$("#moreInfo").click(function () {
	    location.href = 'moreinfo.html';
	});
}

/**
  * Handle run tests click.
  */
function handleRunTestsEvent() {
	$("#runTests").click(function () {
	    location.href = 'test/testrunner.html';
	});
}

/**
  * Echits approval tests click.
  */
function handleEthicsAppEvent() {
	$("#ethicsApp").click(function () {
	    location.href = 'ethics.html';
	});
}

handleMoreInfoEvent();
handleRunTestsEvent();
handleEthicsAppEvent();