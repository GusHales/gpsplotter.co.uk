//global declaration of map and other variables
var map = new google.maps.Map(document.getElementById('map-canvas'), {
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
}),
    markers = [],
    cityCircles = [],
    cityCircle,
    marker,
    r,
    i,
    date,
    time,
    currentDate,
    b1 = $("#btn1")[0], b2 = $("#btn2")[0], b3 = $("#btn3")[0],
    b4 = $("#btn4")[0], b5 = $("#btn5")[0], b6 = $("#btn6")[0],
    counter = 0;

/**
  * Set markers takes in location, accuracy and content string which is the values which will be displayed in info window.
  * @param {int} location - The gps location.
  * @param {int} acc - The gps accuracy.
  * @param {string} contentString - The content for infowindow.
  */
function setMarkers(location, acc, contentString) {
    'use strict';

    var accuracy = parseInt(acc, 10),
        currentLocation = {
            position: location, map: map
        },
        accuracyCircle = {strokeColor: '#619fdd', strokeOpacity: 0.8, strokeWeight: 2, 
        fillColor: '#619fdd', fillOpacity: 0.35, map: map, center: location, radius: accuracy
        },
        infowindow = new google.maps.InfoWindow({
            content: contentString
        });
    marker = new google.maps.Marker(currentLocation);
    cityCircle = new google.maps.Circle(accuracyCircle);
    cityCircles.push(cityCircle);
    marker.setOpacity(1.0);
    markers.push(marker);
    infowindow.open(map,marker);
    setTimeout(function () {
        for (i = 0; i < markers.length; i += 1) {
            markers[i].setOpacity(0.25);
            cityCircles[i].setMap(null);
            infowindow.close(map,marker);
        }
    }, 5000);
    
    google.maps.event.addListener(marker, 'click', (function (marker, cityCircle) {
        return function () {
            marker.setOpacity(1.0);
            infowindow.open(map,marker);
            var accuracyCircle = {strokeColor: '#619fdd', strokeOpacity: 0.8, strokeWeight: 2, 
            fillColor: '#619fdd', fillOpacity: 0.35, map: map, center: location, radius: acc
            };
            setTimeout(function () {
                marker.setOpacity(0.25);
                cityCircle.setMap(null);
                infowindow.close(map,marker);
            }, 5000);
            cityCircle.setOptions(accuracyCircle);
        };
    })(marker, cityCircle));
}

/**
  * Handle error with geolocation.
  * @param {boolean} errorFlag - The gps location.
  */
function handleNoGeolocation(errorFlag) {
    'use strict';
    var content,
        options,
        infowindow;
    if (errorFlag) {
        content = 'Error: The Geolocation service failed.';
    } else {
        content = 'Error: Your browser doesn\'t support geolocation.';
    }

    options = {
        map: map,
        position: new google.maps.LatLng(60, 105),
        content: content
    };

    infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
}

/**
  * Once the map is ready to be loaded this will be called, this success call is for location.
  * @param {int} location - The gps location.
  */
function locationSuccess(location) {
    'use strict';
    var loc = new google.maps.LatLng(location.coords.latitude,
                               location.coords.longitude),
        lats = location.coords.latitude,
        longs = location.coords.longitude,
        accs = location.coords.accuracy,
        speed = location.coords.speed,
        lats = lats.toString().substring(0,7),
        longs = longs.toString().substring(0,7),
        accs = accs.toString().substring(0,2),
        speed = speed*2.23693629,
        speed = Math.round(speed);

    document.getElementById('latText').value = "";
    document.getElementById('longText').value = "";
    document.getElementById('accText').value = "";
    document.getElementById('speedText').value = "";



    document.getElementById('latText').value = lats;
    document.getElementById('longText').value = longs;
    document.getElementById('accText').value = accs;
    document.getElementById('speedText').value = speed;


    time = location.timestamp;
    date = new Date(time);
    currentDate = date.toString().substring(4,24);
    
    document.getElementById('timeText').value = currentDate;
    document.getElementById('millDateText').value = time;

    if (counter !== 1) {
        counter = 1;
        map.setCenter(loc);
    }
}

/**
  * This is another success call but for the heading.
  * @param {int} heading - The heading of the user(compass).
  */
function headingSuccess(heading) {  
    var heading = heading.magneticHeading,
        heading = Math.floor(heading);

        console.log(heading);

    document.getElementById('headText').value = "";
    document.getElementById('headText').value = heading;
};

/**
  * Handle error with heading.
  * @param {string} compassError - The compass error message.
  */
function headingError(compassError) {
    alert('Compass error: ' + compassError.code);
};

/**
  * Handle low accuracy error.
  * @param {int} position - The compass error message.
  */
function errorCallback_lowAccuracy(position) {
    'use strict';
    var msg = "Can't get your location, Error = ";
    if (error.code === 1) {
        msg += "PERMISSION_DENIED";
    } else if (error.code === 2) {
        msg += "POSITION_UNAVAILABLE";
    } else if (error.code === 3) {
        msg += "TIMEOUT";
    }
    msg += ", msg = " + error.message;
    alert(msg);
}

/**
  * Handle hight accuracy error.
  * @param {int} position - The compass error message.
  */
function errorCallback_highAccuracy(position) {
    'use strict';
    if (error.code === error.TIMEOUT) {
        // Attempt to get GPS loc timed out after 5 seconds, 
        // try low accuracy location
        alert("attempting to get low accuracy location");
        navigator.geolocation.getCurrentPosition(
            locationSuccess,
            errorCallback_lowAccuracy,
            {maximumAge: 600000, timeout: 10000, enableHighAccuracy: false}
        );
        return;
    }
    
    var msg = "Can't get your location, Error = ";
    if (error.code === 1) {
        msg += "PERMISSION_DENIED";
    } else if (error.code === 2) {
        msg += "POSITION_UNAVAILABLE";
    }
    msg += ", msg = " + error.message;
    alert(msg);
}

/**
  * Map initializer for location and heading.
  */
function initialize() {
    'use strict';
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(locationSuccess, errorCallback_highAccuracy, {aximumAge: 3000, timeout: 5000, enableHighAccuracy: true});
        navigator.compass.watchHeading(headingSuccess, headingError, {frequency: 3000});
    } else {
    // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
}

/**
  * Check the current conncetion status.
  * @returns {array} states
  */
function checkConnection() {
    'use strict';
    var networkState = navigator.connection.type,
        states = {};
    states[Connection.UNKNOWN]  = 'Connected';
    states[Connection.ETHERNET] = 'Connected';
    states[Connection.WIFI]     = 'Connected';
    states[Connection.CELL_2G]  = 'Connected';
    states[Connection.CELL_3G]  = 'Connected';
    states[Connection.CELL_4G]  = 'Connected';
    states[Connection.CELL]     = 'NotConnected';
    states[Connection.NONE]     = 'NotConnected';

    return states[networkState];
}

/**
  * Generates research id.
  * @returns {int}
  */
function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

/**
  * Generate event id.
  * @returns {int}
  */
function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

/**
  * Handle post research, every time a button is pressed this ajax call will send data to the server.
  * @returns {boolean}
  */
function postResearch() {
    'use strict';
    var latText = $('#latText').val(),
        longText = $('#longText').val(),
        timeText = $('#timeText').val(),
        millDateText = $('#millDateText').val(),
        accText = $('#accText').val(),
        headingText = $('#headText').val(),
        speedText = $('#speedText').val(),
        researchId = $('#researchId').val(),
        userId = JSON.parse(localStorage.getItem('userId')) || "",
        btnText = $('#btnText').val(),
        resName = JSON.parse(localStorage.getItem('researchName')) || [],
        btnDesc = $('#btnDesc').val(),
        eventId = $('#eventId').val();
       
    $.ajax({
        type: 'POST',
        dataType: "text",
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/populateresearch.php',
        data: {postUserId: userId, postResearchId: researchId, postLatText: latText, postLongText: longText, postTimeText: timeText, postAccText: accText, postBtnText: btnText,
                             postBtnDesc: btnDesc, postEventId: eventId, postMillDateText: millDateText, postHeadingText: headingText, postSpeedText: speedText},
        success: function (response) {
            console.log(response);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
    return false;
}

/**
  * Concept keyboard action.
  */
function handleConceptKeyboardEvent() {
    $([b1, b2, b3, b4, b5, b6]).click(function () {
        'use strict';
        var eventId = generateEventId(),
            userId = JSON.parse(localStorage.getItem('userId')) || "",
            researchedDataArray = JSON.parse(localStorage.getItem('researchedDataArray')),
            latText = $('#latText').val(),
            longText = $('#longText').val(),
            accText = $('#accText').val(),
            timeText = $('#timeText').val(),
            headText = $('#headText').val(),
            speedText = $('#speedText').val(),
            loc = new google.maps.LatLng(latText, longText),
            connectionStatus = checkConnection(),
            contentString = '<p>Button = ' + this.innerHTML + 
                            '<br>Latitude = ' + latText +
                            '<br>Longitude = ' + longText +
                            '<br>Accuracy = ' + accText +
                            '<br>Time = ' + timeText +
                            '<br>Heading(degrees) = ' + headText +
                            '<br>Speed(mph) = ' + speedText + '</p>';

        document.getElementById('eventId').value = eventId;
        document.getElementById('userId').value = userId;


        setMarkers(loc, accText, contentString);
        map.setCenter(loc);
        
        if (connectionStatus !== 'NotConnected') {
            document.getElementById("result").innerHTML = "Data saved";
            postResearch();
            $.getScript('js/cacher.js', function () {
                cacherPost();
            });
        } else {
            //call cacher script
            document.getElementById("result").innerHTML = "Data has been cached";
            $.getScript('js/cacher.js', function () {
                dataCacher();
            });
        }
    });
}

/**
  * Done button action.
  */
function handleDoneEvent() {
    $("#doneButton").click(function () {
        'use strict';
        r = confirm("Press OK if you're done with the research and which to exit research or Cancel if you're not done.");
        if (r === true) {
            $.getScript('js/cacher.js', function () {
                cacherPost();
            });
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '[]');
            localStorage.setItem('researchId', '[]');
            localStorage.setItem('researchedDataArray', '[]');
            setTimeout(function () {location.href = 'private.html'; }, 3000);
        }
    });
}

/**
  * Once new research is pressed the data will be cleared.
  */
function handleNewResearchEvent() {
    $('#newResearch').click(function () {
        'use strict';
        r = confirm("Press OK if you're done with the research and which to exit research or Cancel if you're not done.");
        if (r === true) {
            location.href = 'newresearch.html';
            var researchId = generateResearchId();
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '""');
            localStorage.setItem('researchId', researchId);
            localStorage.setItem('researchedDataArray', '[]');
        }
    });
}

/**
  * Logout will clear any local storage.
  */
function handleLogoutEvent() {
    $('#logout').click(function () {
        'use strict';
        r = confirm("Press OK if you're done with the research and which to exit research or Cancel if you're not done.");
        if (r === true) {
            location.href = 'index.html';
            localStorage.setItem('buttonsArray', '[]');
            localStorage.setItem('buttonsNameArray', '[]');
            localStorage.setItem('buttonsDescArray', '[]');
            localStorage.setItem('researchName', '[]');
            localStorage.setItem('researchedDataArray', '[]');
            localStorage.setItem('researchId', '[]');
            localStorage.setItem('loginReply', '""');
            localStorage.setItem('username', '""');
            localStorage.setItem('userId', '""');
            localStorage.setItem('researchNamesArray', '[]');
            localStorage.setItem('researchIdsArray', '[]');
        }
    });
}

/**
  * Redirect to private.html.
  */
function handleAccountEvent() {
    $('#account').click(function () {
        'use strict';
        r = confirm("Press OK if you're done with the research and which to exit research or Cancel if you're not done.");
        if (r === true) {
            location.href = "private.html";
        }
    });
}

handleConceptKeyboardEvent();
handleDoneEvent();
handleNewResearchEvent();
handleLogoutEvent();
handleAccountEvent();

//-- Map settings --//
google.maps.event.addDomListener(window, 'resize', initialize);
google.maps.event.addDomListener(window, 'load', initialize);
