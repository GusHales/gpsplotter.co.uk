/**
  * Ajax post the new research that has been created.
  * @return {ajaxObject}
  */
function postNewResearch() {
    'use strict';
    var researchId = localStorage.researchId,
        researchName = JSON.parse(localStorage.getItem('researchName')) || [],
        buttonsNumber = localStorage.getItem('buttonsArray') || [],
        buttonsCreated = localStorage.getItem('buttonsNameArray') || [],
        buttonsCreatedLongName = localStorage.getItem('buttonsDescArray') || [],
        eventId = generateEventId(),
        userId = JSON.parse(localStorage.getItem('userId')) || "",
        researchName = sanitizer(researchName[0].toString()),
        date = new Date(),
        timeInMill = date.getTime(),
        finalDate = new Date(timeInMill);

    return $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/setresearch.php',
        data: {postSetResearchId: researchId, postSetResearchName: researchName.toString(), postSetButtonsNumber: buttonsNumber, postSetButtonsCreated: buttonsCreated, postSetButtonsCreatedLongName: buttonsCreatedLongName, postSetEventId: eventId, postSetUserId: userId, postTime: finalDate, postTimeInMill: timeInMill},
        success: function (response, status, xhr) {
            var responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            console.log(response);
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

/**
  * This function will call the table sorting framework once the ajax call has been made.
  */
function waitForAJAX() {
    var researchName = JSON.parse(localStorage.getItem('researchName')) || "";
    if(researchName !== "") {
        $.when(postNewResearch()).done(function(a1) {
        'use strict';
        var oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
            oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
            oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
            
            finalButtonArray = [],
            finalButtonDescArray = [],
            i;
            console.log(researchName);
        
            if (oldButtons.length >= 1) {
                location.href = 'research.html';
                for (i = 0; i < oldButtons.length; i += 1) {
                    finalButtonArray.push(oldButtons[i]);
                    finalButtonArray.push(oldButtonsName[i]);
                    finalButtonDescArray.push(oldButtons[i]);
                    finalButtonDescArray.push(oldButtonsDesc[i]);
                }
                localStorage.setItem('finalButtonArray', JSON.stringify(finalButtonArray));
                localStorage.setItem('finalButtonDescArray', JSON.stringify(finalButtonDescArray));
            } else {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: * Please enter required fields.";
            } 
        });
    } else {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: * You must add a research name.";
    }
}

/**
  * handle create research button click.
  */
function handleCreateResearchEvent() {
    $("#createResearch").click(function () {
        waitForAJAX();
    });
}

/**
  * Handle remove button on table.
  */
function handleRemoveButtonEvent() {
    $(document).on('click', '.input-remove-row', function () {
        'use strict';
        var tr = $(this).closest('tr'),
            oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
            oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
            oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
            i,
            remover;
        
        tr.fadeOut(200, function () {
            tr.remove();
            remover = tr[0].innerText;
            for (i = 0; i < oldButtons.length; i += 1) {
                if (remover.indexOf(tr[0].innerText.substring(0, 4)) > -1) {
                    if (oldButtons[i] === tr[0].innerText.substring(0, 4)) {
                        oldButtons.splice(i, 1);
                        oldButtonsName.splice(i, 1);
                        oldButtonsDesc.splice(i, 1);
                    }
                }
                localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
                localStorage.setItem('buttonsNameArray', JSON.stringify(oldButtonsName));
                localStorage.setItem('buttonsDescArray', JSON.stringify(oldButtonsDesc));
            }
    	});
    });
}

/**
  * Navigation for the form.
  */
function handleNavigation() {
    $(function () {
        'use strict';
        $("#researchName").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("buttons").focus();
            }
        });
         
        $("#buttonName").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("buttonDesc").focus();
            }
        });
        
        $("#buttonDesc").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("addButton").focus();
            }
        });
    });
}

/**
  * Handle cross button on research name.
  */
function handleResearchNameClearEvent() {
    $("#searchclear").click(function () {
        'use strict';
        document.getElementById('researchName').readOnly = false;
        $("#researchName").val('');
        localStorage.setItem('researchName', '""');
    });
}

/**
  * JQuery function because it had a performace boost, this function will handle the add button.
  */
function handleAddButtonEvent() {
    $(function () {
        'use strict';
        $('.preview-add-button').click(function () {
            var formData = {},
                researchName = JSON.parse(localStorage.getItem('researchName')) || "",
                oldButtons = JSON.parse(localStorage.getItem('buttonsArray')) || [],
                oldButtonsName = JSON.parse(localStorage.getItem('buttonsNameArray')) || [],
                oldButtonsDesc = JSON.parse(localStorage.getItem('buttonsDescArray')) || [],
                row;
        
            if ($.inArray($('.research-form #buttons option:selected').text(), oldButtons) !== -1) {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.research-form #buttons option:selected').text()  + " button already added.";
            } else {
                if ($('.research-form #buttons option:selected').text() !== "Select") {
                    document.getElementById("errorPane").style.visibility = "hidden";
                    document.getElementById("errorMsg").style.visibility = "hidden";
                    if($('.research-form input[name="researchName"]').val() === "") {
                        document.getElementById("errorPane").style.visibility = "visible";
                        document.getElementById("errorMsg").style.visibility = "visible";
                        document.getElementById("errorMsg").innerHTML = "Error: Please enter a research name."; 
                    } else {
                        if ($('.research-form input[name="buttonName"]').val() !== "") {
                            formData.buttons = $('.research-form #buttons option:selected').text();
                            formData.buttonName = sanitizer($('.research-form input[name="buttonName"]').val().toString());
                            formData.buttonDesc = sanitizer($('.research-form input[name="buttonDesc"]').val().toString());
                            formData["remove-row"] = '<span class="glyphicon glyphicon-remove"></span>';
                            oldButtonsName.push(sanitizer($('.research-form input[name="buttonName"]').val().toString()));
                            oldButtons.push($('.research-form #buttons option:selected').text());
                            oldButtonsDesc.push(sanitizer($('.research-form input[name="buttonDesc"]').val().toString()));
                            researchName = [];
                            console.log(sanitizer($('.research-form input[name="buttonDesc"]').val().toString()));
                            researchName.push(sanitizer($('.research-form input[name="researchName"]').val().toString()));
                        } else {
                            document.getElementById("errorPane").style.visibility = "visible";
                            document.getElementById("errorMsg").style.visibility = "visible";
                            document.getElementById("errorMsg").innerHTML = "Error: * Please enter required fields.";
                        }
                    }
                    if (researchName !== "") {
                        document.getElementById("researchName").readOnly = true;
                        document.getElementById("buttonName").value = "";
                        document.getElementById("buttonDesc").value = "";
                    } else {
                        document.getElementById("errorPane").style.visibility = "visible";
                        document.getElementById("errorMsg").style.visibility = "visible";
                        document.getElementById("errorMsg").innerHTML = "Error: * Please enter required fields.";
                    }
                } else {
                    document.getElementById("errorPane").style.visibility = "visible";
                    document.getElementById("errorMsg").style.visibility = "visible";
                    document.getElementById("errorMsg").innerHTML = "Error: * Please enter required fields.";
                }
            }
            
            row = $('<tr></tr>');
            $.each(formData, function (type, value) {
                $('<td class="input-' + type + '"></td>').html(value).appendTo(row);
            });
            $('.preview-table > tbody:last').append(row);
            
            localStorage.setItem('researchName', JSON.stringify(researchName));
            localStorage.setItem('buttonsArray', JSON.stringify(oldButtons));
            localStorage.setItem('buttonsNameArray', JSON.stringify(oldButtonsName));
            localStorage.setItem('buttonsDescArray', JSON.stringify(oldButtonsDesc));
        });
    });
}

handleCreateResearchEvent();
handleRemoveButtonEvent();
handleNavigation();
handleResearchNameClearEvent();
handleAddButtonEvent();