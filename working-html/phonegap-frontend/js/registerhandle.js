/**
  * Function which generates a unique user_id.
  * @return {int}
  */
function generateUserId() {
    userId = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });
    return userId;
}

/**
  * Ajax post register function, posts the data to the server and once the server recives/handles it a reply will be sent back to the client.
  * @return {ajaxObject}
  */
function postRegister() {
    'use strict';
    var username = sanitizer($('#username').val()),
        email = sanitizer($('#email').val()),
        password = $('#password').val(),
        userId = generateUserId();

    return $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
        success: function (response, status, xhr) {
            //clean response message
            var responseClean = response.replace(/(?:\r\n|\s|\r|\n)/g, '');
            localStorage.setItem('loginReply', JSON.stringify(responseClean));

            //handle the reply message from the server and display the apropriate message
            if (responseClean === "enterUsername") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Please enter a username. ";
            } else if (responseClean === "enterPassword") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Please enter a password. ";
            } else if (responseClean === "invalidEmail") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Please enter a valid email address. ";
            } else if (responseClean === "usernameInUse") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Username already in use. ";
            } else if (responseClean === "emailInUse") {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Email address already in use. ";
            } else if (responseClean === "login.html") {
                location.href = responseClean;
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

/**
  * This function will call the table sorting framework once the ajax call has been made.
  */
function waitForAJAX() {
    $.when(postRegister()).done(function(a1) {
        hideSpinner();
    });

}

/**
  * Handle register button click.
  */
function handleRegisterEvent() {
    $('#registerButton').click(function () {
        'use strict';
        showSpinner();
        waitForAJAX();
    });
}

/**
  * Handle register navitation.
  */
function handleNavigation() {
    $(function () {
        'use strict';
        $("#username").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("email").focus();
            }
        });
        
        $("#email").change(function () {
            document.getElementById("password").focus();
        });
        
        $("#password").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("registerButton").focus();
            }
        });
        
        $("#buttonDesc").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("addButton").focus();
            }
        });
    });
}

/**
  * Show AJAX spinners.
  */
function showSpinner(){
    $.mobile.loading("show");
}

/**
  * Hide AJAX spinners.
  */
function hideSpinner(){
    $.mobile.loading("hide");
}

handleRegisterEvent();
handleNavigation();