/**
  * Handle back button on android.
  */
function onBackKeyDown() {
    'use strict';
    location.href = "login.html";
}

/**
  * Handle device loaded on android.
  */
function onDeciveReady() {
    'use strict';
    document.addEventListener("backbutton", onBackKeyDown, false);
}

/**
  * This function gets called first, body calls this onload.
  */
function onLoad() {
    'use strict';
    document.addEventListener("deviceready", onDeciveReady);
}