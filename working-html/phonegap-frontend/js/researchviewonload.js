var glasLocations = [],
    mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(55.8580, -4.2590)
    },
    map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

/**
  * Function which sets markers and allows them to be clickable. takes in location which is an an array of all data from the location where it was taken and a google map.
  * @param {string} myLatLng - All the data gathered by the user.
  * @param {string} contentString - Content for infowindow for the map.
  */
function setMarkers(myLatLng, contentString) {
    'use strict';
    var i,
        locs,
        locssplit,
        myLatLng,
        marker,
        infowindow,
        contentString,
        markers = [];

        marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            opacity: 0.2
        });

        infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        google.maps.event.addListener(marker, 'click', (function (marker) {
            return function () {
                marker.setOpacity(1.0);
                infowindow.open(map,marker);
                setTimeout(function () {
                    marker.setOpacity(0.25);
                    infowindow.close(map,marker);
                }, 10000);
            };
        })(marker));

}

/**
  * Initialize map using google maps javascript v3.
  */
function initialize() {
    'use strict';
    console.log("here");
    var i,
        locs,
        locssplit;
        for (i = 0; i < glasLocations.length; i += 1) {
        
        locs = glasLocations[i];
        locssplit = locs.split("|");
        if(locssplit[0] !== "Run") {
            if(locssplit[0] !== "ResearchCreated") {
                var myLatLng = new google.maps.LatLng(locssplit[0], locssplit[1]);
                var contentString = '<p>Button = ' + locssplit[7] + 
                                    '<br>Latitude = ' + locssplit[0] +
                                    '<br>Longitude = ' + locssplit[1] +
                                    '<br>Accuracy = ' + locssplit[2] +
                                    '<br>Time = ' + locssplit[5] +
                                    '<br>Heading(degrees) = ' + locssplit[3] +
                                    '<br>Speed(mph) = ' + locssplit[4] + '</p>';
                setMarkers(myLatLng,contentString);
            }
        }
    }
}

/**
  * Load the research data and display it on a table.
  */
function viewPost() {
    'use strict';
    var researchId = JSON.parse(localStorage.getItem('viewResearchId')) || "",
        responseClean,
        responseArray,
        i,
        temparray,
        chunk,
        j;

    return $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/viewresearch.php',
        data: {postViewResearch: researchId},
        success: function (response, status, xhr) {
            responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
            responseArray = responseClean.split("|");
            responseArray.pop();

            localStorage.setItem('exportDataArray', JSON.stringify(responseArray));
            chunk = 9;
            j = 0;

            for (i = 0; i < responseArray.length; i += chunk) {
                    temparray = responseArray.slice(i, i + chunk);
                    if (temparray[0] !== "") {
                        glasLocations.push(temparray[0] + "|" + temparray[1] + "|" + temparray[2] + "|" + temparray[3] + "|" + temparray[4] + "|" + temparray[5] + "|" + temparray[6] + "|" + temparray[7] + "|" + temparray[8]);
                        $('#addr' + j).html(" <td><h4 name='" + temparray[0] + "' id='researchName'>" + temparray[0] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[1] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[2] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[3] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[4] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[5] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[6] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[7] + "</h4></td><td><h4 name='researchName' id='researchName'>" + temparray[8] + "</h4></td>");
                        $('#myTable').append('<tr id="addr' + (j + 1) + '"></tr>');

                        j += 1;
                    }
            }
            
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

/**
  * This function will call the table sorting framework once the ajax call has been made.
  */
function waitForAJAX() {
    $.when(viewPost()).done(function(a1) {
        $("#myTable").tablesorter({
            sortReset   : true,
            sortRestart : true,
            sortInitialOrder: 'desc',
            sortList: [[6,0]],
            headers : {
              3 : { sortInitialOrder: 'asc' },
            }
        });
        //then the map gets called and populated
        document.getElementById("researchName").innerHTML = JSON.parse(localStorage.getItem('viewResearchName')) || "";
        initialize();
    });

}

/**
  * Handle back button.
  */
function onBackKeyDown() {
    'use strict';
    location.href = "private.html";
}

/**
  * Once device is ready function.
  */
function onDeciveReady() {
    'use strict';
    document.addEventListener("backbutton", onBackKeyDown, false);
    waitForAJAX();
}

/**
  * Body lodas calls this.
  */
function onLoad() {
    'use strict';
    document.addEventListener("deviceready", onDeciveReady);
}