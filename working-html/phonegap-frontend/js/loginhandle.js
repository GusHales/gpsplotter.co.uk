/**
  * Ajax post login function, posts the data to the server and once the server recives/handles it a reply will be sent back to the client.
  * @return {ajaxObject}
  */
function postLogin() {
    'use strict';
    var username,
        password,
        responseClean,
        responseArray;

        username = sanitizer($('#username').val());
        password = sanitizer($('#password').val());
     
    return $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/login.php',
        data: {postUsername: username, postPassword: password},
    
        success: function (response, status, xhr) {
            //clean up response text
            responseClean = response.replace(/(?:\r\n|\r|\n)/g, '');
        
            localStorage.setItem('loginReply', JSON.stringify(responseClean));
            localStorage.setItem('username', JSON.stringify(username));

            if (responseClean.indexOf("error") > -1) {
                document.getElementById("errorPane").style.visibility = "visible";
                document.getElementById("errorMsg").style.visibility = "visible";
                document.getElementById("errorMsg").innerHTML = "Error: " + $('.payment-form #buttons option:selected').text()  + " Login failed.";
            } else if (responseClean.indexOf("private.html") > -1) {
                responseArray = responseClean.split(" ");
                localStorage.setItem('userId', JSON.stringify(responseArray[1]));
                document.getElementById("errorPane").style.visibility = "hidden";
                document.getElementById("errorMsg").style.visibility = "hidden";
                location.href = responseArray[0];
            }
        },
        error: function (xhr, status, error) {
            console.log(xhr);
            console.log(status);
            console.log(error);
        }
    });
}

/**
  * This function will call the table sorting framework once the ajax call has been made.
  */
function waitForAJAX() {
    $.when(postLogin()).done(function(a1) {
        hideSpinner();
    });
}

/**
  * Handle home button by cleaning artifacts and loading index, done in the login screen.
  */
function handleLoginHomePageEvent() {
    $('#homePage').click(function () {
        'use strict';
        location.href = 'index.html';
        localStorage.setItem('buttonsArray', '[]');
        localStorage.setItem('buttonsNameArray', '[]');
        localStorage.setItem('buttonsDescArray', '[]');
        localStorage.setItem('researchName', '[]');
        localStorage.setItem('researchId', '[]');
        localStorage.setItem('researchedDataArray', '[]');
    });
}

/**
  * Handle log in button by cleaning artifacts and calling postLogin() function, done in the login screen.
  */
function handleLoginLogInEvent() {
    $("#loginButton").click(function (e) {
        'use strict';   
        showSpinner();
        e.preventDefault();
        localStorage.setItem('buttonsArray', '[]');
        localStorage.setItem('buttonsNameArray', '[]');
        localStorage.setItem('buttonsDescArray', '[]');
        localStorage.setItem('researchName', '[]');
        localStorage.setItem('researchedDataArray', '[]');
        localStorage.setItem('researchId', '[]');
        localStorage.setItem('loginReply', '""');
        localStorage.setItem('username', '""');
        localStorage.setItem('researchNamesArray', '[]');
        localStorage.setItem('researchIdsArray', '[]');
        waitForAJAX();
    });
}

/**
  * Handle register click, done in the login screen.
  */
function handleLoginRegisterEvent() {
    $("#register").click(function () {
        'use strict';
        location.href = "register.html";
    });
}

/**
  * Navigation handles, if the user presses enter it'll move them to the next field.
  */
function handleNavigationEvents() {
    $(function () {
        'use strict';
        $("#username").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("password").focus();
            }
        });
         
        $("#password").keydown(function (e) {
            if (e.keyCode === 13) {
                document.getElementById("loginButton").focus();
            }
        });
    });
}

handleLoginHomePageEvent();
handleLoginLogInEvent();
handleLoginRegisterEvent();
handleNavigationEvents();