var researchView = "";

function researchViewPostAjax() {
	var researchId = "1lhg0u8rb";

	var xhr = $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/viewresearch.php',
        data: {postViewResearch: researchId}
     })
	.always(function(data, status){
		console.log(data);
		researchView = data;
	});
}

function researchViewTester() {
	asyncTest('researchViewPostTest', function(){
		expect(1); // we have one async test to run
		equal(researchView, "ResearchCreated|||||Tue Mar 31 2015 22:37:37 GMT+0100 (BST)|1427837857835|||55.8634|-4.2410|56|267|0|Mar 31 2015 22:37:44|1427837864969|hello||\n", 'Data is retriaved from the server.');
		start();
	});
}

researchViewPostAjax();

setTimeout(function() {
	researchViewTester();
},5000);