var successRegister = "";
var successRegisterEnterUsername = "";
var successRegisterEnterPassword = "";
var successRegisterInvalidEmail = "";
var successRegisterInUse = "";
var successRegisterEmailInUse = "";

function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

function registerEntry() {
    var username = generateResearchId(),
        email = generateResearchId()+"@test.com",
        password = "test",
        userId = generateUserId();

		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
	        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
		})
		.always(function(data, status){
			console.log(data);
			successRegister = data;
		});
}

function registerUsernameEnterUsername() {
    var username = "",
        email = "test432@test432.com",
        password = "test",
        userId = generateUserId();

		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
	        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
		})
		.always(function(data, status){
			console.log(data);
			successRegisterEnterUsername = data;
		});
}

function registerUsernameEnterPassword() {
    var username = generateResearchId(),
        email = generateResearchId()+"@test432.com",
        password = "",
        userId = generateUserId();

		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
	        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
		})
		.always(function(data, status){
			console.log(data);
			successRegisterEnterPassword = data;
		});
}

function registerUsernameInvalidEmail() {
    var username = generateResearchId(),
        email = "123",
        password = "test",
        userId = generateUserId();

		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
	        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
		})
		.always(function(data, status){
			console.log(data);
			successRegisterInvalidEmail = data;
		});
}

function registerUsernameInUse() {
    var username = "test432",
        email = "test432@test432.com",
        password = "test432",
        userId = generateUserId();

		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
	        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
		})
		.always(function(data, status){
			console.log(data);
			successRegisterInUse = data;
		});
}

function registerEmailInUse() {
    var username = generateResearchId(),
        email = "test432@test432.com",
        password = "test",
        userId = generateUserId();

		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/register.php',
	        data: {postUserId: userId, postUsername: username, postEmail: email, postPassword: password},
		})
		.always(function(data, status){
			console.log(data);
			successRegisterEmailInUse = data;
		});
}



function registerTester() {
	asyncTest('testRegisterFunctionality', function(){
		expect(1); // we have one async test to run
		equal(successRegister, "login.html \n", 'Test functionlaity of the register screen.');
		start();
	});

	asyncTest('testRegisterMessageEnterUsername', function(){
		expect(1); // we have one async test to run
		equal(successRegisterEnterUsername, "enterUsername \n", 'Test to see if the right message is returned.(enterusername message)');
		start();
	});

	asyncTest('testRegisterMessageEnterPassword', function(){
		expect(1); // we have one async test to run
		equal(successRegisterEnterPassword, "enterPassword \n", 'Test to see if the right message is returned.(enterpassword message)');
		start();
	});

	asyncTest('testRegisterMessageInvalidEmail', function(){
		expect(1); // we have one async test to run
		equal(successRegisterInvalidEmail, "invalidEmail \n", 'Test to see if the right message is returned.(invalidemail message)');
		start();
	});

	asyncTest('testRegisterMessageUsernameInUse', function(){
		expect(1); // we have one async test to run
		equal(successRegisterInUse, "usernameInUse \n", 'Test to see if the right message is returned.(inuse message)');
		start();
	});

	asyncTest('testRegisterMessageEmailInUse', function(){
		expect(1); // we have one async test to run
		equal(successRegisterEmailInUse, "emailInUse \n", 'Test to see if the right message is returned.(emailinuse message)');
		start();
	});
}

registerEntry();
registerUsernameEnterUsername();
registerUsernameEnterPassword();
registerUsernameInvalidEmail();
registerUsernameInUse();
registerEmailInUse();

setTimeout(function() {
	registerTester();
},10000);