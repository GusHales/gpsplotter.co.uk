var privatePostReply = "";

function testPrivateHandleRun() {
	var result = handleRun("testing");
	console.log(result);
	QUnit.test("handleRun", function( assert ) {
		assert.equal(true, result, "handleRun returned true;");
	});
}

function privatePostAjax() {
	var userId = "dee5b321-585b-4433-858c-06365f1d4b0c";

	var xhr = $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/getresearch.php',
        data: {postUserId: userId}
     })
	.always(function(data, status){
		console.log(data);
		privatePostReply = data;
	});
}

function privatePostTester() {
	asyncTest('privatePostTest', function(){
		expect(1); // we have one async test to run
		equal(privatePostReply, "1lhg0u8rb|hello|\n", 'Data is retriaved from the server.');
		start();
	});
}

privatePostAjax();
testPrivateHandleRun();

setTimeout(function() {
	privatePostTester();
},5000);