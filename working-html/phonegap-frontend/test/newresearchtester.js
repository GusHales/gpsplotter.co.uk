var successArrayNewResearch = [];

function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

function newResearchInsert() {
	'use strict';
	for(var i = 0; i < 1; i++){
	    var researchId = generateResearchId(),
	        buttonsNumber = "btn1",
	        buttonsCreated = "btn1",
	        buttonsCreatedLongName = "btn1",
	        eventId = generateEventId(),
	        userId = "b34245ce-2c66-4953-9e24-21ff4d44e83b",
	        researchName = "test",
	        date = new Date(),
	        timeInMill = date.getTime(),
	        finalDate = new Date(timeInMill);

	    var xhr = $.ajax({
		type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/setresearch.php',
        data: {postSetResearchId: researchId, postSetResearchName: researchName.toString(), postSetButtonsNumber: buttonsNumber, 
        	postSetButtonsCreated: buttonsCreated, postSetButtonsCreatedLongName: buttonsCreatedLongName, postSetEventId: eventId, postSetUserId: userId, postTime: finalDate, postTimeInMill: timeInMill}
		})
		.always(function(data, status){
			successArrayNewResearch.push(data);
		});
	}
}

function newResearchInsertTester() {
	asyncTest('newResearchCreateTest', function(){
		expect(1); // we have one async test to run
		console.log(successArrayNewResearch.length);
		equal(successArrayNewResearch.length, 1, 'New research has been added, and the test has passed.');
		start();
	});
}

newResearchInsert();


setTimeout(function() {
	newResearchInsertTester();
},5000);