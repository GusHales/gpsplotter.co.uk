//once new research is pressed the data will be cleared.
function homePage() {
    'use strict';
    //once home is pressed the data will be cleared.
    $('#testHomePage').click(function () {
        location.href = '../../index.html';
    });    
    $('#refreshTest').click(function () {
        location.href = "testrunner.html";
    });
     
}

homePage();

function onBackKeyDown() {
    'use strict';
    location.href = "../../index.html";
}

function onDeciveReady() {
    'use strict';
    document.addEventListener("backbutton", onBackKeyDown, false);
}

function onLoad() {
    'use strict';
    document.addEventListener("deviceready", onDeciveReady);
}

onLoad();
