var successArrayLogIn = [];
var successLogInErrorHandle = "";

function loginEntry() {
	for(var i = 0; i < 5; i++){
	    var username,
	        password;

	        username = "test";
	        password = "test";


		var xhr = $.ajax({
	        type: 'POST',
	        dataType: 'text',
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/login.php',
	        data: {postUsername: username, postPassword: password}
		})
		.always(function(data, status){
			successArrayLogIn.push(data);
			console.log(successArrayLogIn);
		});
	}
}

function loginError() {
    var username,
        password;

        username = "test";
        password = "1234";


	var xhr = $.ajax({
        type: 'POST',
        dataType: 'text',
        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/login.php',
        data: {postUsername: username, postPassword: password}
	})
	.always(function(data, status){
		console.log(data + "here");
		successLogInErrorHandle = data;
	});
}


function loginEntryTester() {
	asyncTest('testLoginFunctionality', function(){
		expect(1); // we have one async test to run
		equal(successArrayLogIn.length, 5, 'Test multiple users loggin into the system.');
		start();
	});
	asyncTest('testLoginErrorChecking', function(){
		expect(1); // we have one async test to run
		equal(successLogInErrorHandle, "errorerror \n", 'Test error message reply');
		start();
	});
}

loginEntry();
loginError();

setTimeout(function() {
	loginEntryTester();
},10000);