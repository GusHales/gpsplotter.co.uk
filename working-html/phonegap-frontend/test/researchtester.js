var successArrayResearch = [];

function ajaxTest() {
	asyncTest('postResearchAjax', function(){
		expect(1); // we have one async test to run
		
		var xhr = $.ajax({
			type: 'GET',
			url: 	'../research.html'
		})
		.always(function(data, status){
			var $data = $(data);
			var pageTitle = $data.filter('title').text();
			equal(pageTitle, 'Research', 'Simple test to check AJAX works correctly on the page.');
			start();
		});

	});

}

function generateEventId() {
    'use strict';
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function generateResearchId() {
    'use strict';
    return Math.random().toString(36).substr(2, 9);
}

function researchInsert() {
	'use strict';
	for(var i = 0; i < 1; i++){
	    var latText = "test",
	        longText = "test",
	        timeText = "test",
	        millDateText = "test",
	        accText = "test",
	        headingText = "test",
	        speedText = "test",
	        researchId = generateResearchId(),
	        userId = "b34245ce-2c66-4953-9e24-21ff4d44e83b",
	        btnText = "test",
	        resName = "test",
	        btnDesc = "test",
	        eventId = generateEventId();
	        
	    var xhr = $.ajax({
	        type: 'POST',
	        dataType: "text",
	        url: 'https://devweb2014.cis.strath.ac.uk/~qdb11141/php/populateresearch.php',
	        data: {postUserId: userId, postResearchId: researchId, postLatText: latText, postLongText: longText, postTimeText: timeText, postAccText: accText, postBtnText: btnText,
	                             postBtnDesc: btnDesc, postEventId: eventId, postMillDateText: millDateText, postHeadingText: headingText, postSpeedText: speedText}
		})
		.always(function(data, status){
			successArrayResearch.push(data);
		});
	}
}

function researchInsertTester() {
	asyncTest('stressResearchTest', function(){
		expect(1); // we have one async test to run
		equal(successArrayResearch.length, 1, 'research populated the table and the return messages matches the expected value');
		start();
	});
}

researchInsert();

setTimeout(function() {
	ajaxTest();
	researchInsertTester();
},5000);
