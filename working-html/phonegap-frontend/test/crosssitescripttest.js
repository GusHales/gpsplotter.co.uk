function testXSS(value) {
	var result = sanitizer(value);
	QUnit.test("sanitizer", function( assert ) {
		assert.equal("&amp;&lt;&gt;&quot;", result, "testXSS string is sanitized");
	});
}

testXSS("&<>\"")